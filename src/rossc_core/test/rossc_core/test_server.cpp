#include <iostream>

#include "rossc_core/Server.hpp"
#include "rossc_core/SocketProxy.hpp"
#include "rossc_core/Streamer.hpp"
#include "rossc_core/Time.hpp"

using namespace rossc::core;
std::string buffer;
std::string hdr = "";
std::string trl = "";
class EventHandler : public rossc::core::SocketProxy {
	public:
		void HandleRecv(Socket* caller, Streamer* stream) { 
			Server* server = (Server*)caller;
			printf("[EventHandler] Server received a message:\n");
			stream->Extract(&buffer);
			printf("[EventHandler] Buffer print: %s\n", buffer.c_str());
			server->Send((const char*)"<BounceAll>");
			stream->Clear();
			printf("[EventHandler] Streamer now empty: '%d'\n", 
					stream->Size());
		}
		
		void HandleRecvEndpoint(Socket* caller, Address addr, Streamer* stream) { 
			Server* server = (Server*)caller;
			printf("[EventHandler] Server received a message from %s\n",
					addr.c_str());
			server->SendNot("<BounceOthers>", addr);
		}
};

void RegisterAll(Server* server, EventHandler *handler) {
	Callback_Socket(server->iOnRelease,    handler, HandleRelease);
	Callback_Socket(server->iOnAccept,     handler, HandleAccept);
	Callback_Socket(server->iOnAcceptPeer, handler, HandleAcceptPeer);
	Callback_Socket(server->iOnDrop,       handler, HandleDrop);
	Callback_Socket(server->iOnDropPeer,   handler, HandleDropPeer);
	Callback_Socket(server->iOnRecv,       handler, HandleRecv);
	Callback_Socket(server->iOnRecvPeer,   handler, HandleRecvPeer);
}

int main(void) {
	EventHandler *handler = new EventHandler();

	Server *server;
	
	printf("[Main] Starting server\n");
	server = new Server();
	RegisterAll(server, handler);
	server->Bind("0.0.0.0:5555", 1);

	printf("[Main] Up and running\n");
	TimeValue clock;
	Time::Tic(&clock);
	while(Time::Toc(&clock) < 60000) {
		server->Send("<Multicast>");
		Time::Sleep(2000);
	}

	printf("[Main] Stopping ASync server\n");
	server->Release();
	
	delete server;
	
	return 0;
}

#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Int16.h"

#include "rossc_core/Time.hpp"
#include "rossc_core/TopicProxy.hpp"

using namespace rossc::core;

int main(int argc, char** argv) {

	ros::init(argc, argv, "topic_proxy");
	

	/*** TESTING SERVER ***/
	ros::NodeHandle TestSrv;
	std_msgs::String TestMsg1; 
	std_msgs::String TestMsg2; 
	std_msgs::String TestMsg3; 
	
	ros::Publisher srvpub1 = TestSrv.advertise<std_msgs::String>("listener1", 1000);
	ros::Publisher srvpub2 = TestSrv.advertise<std_msgs::String>("listener2", 1000);
	ros::Publisher srvpub3 = TestSrv.advertise<std_msgs::String>("listener3", 1000);

	TestMsg1.data = "Message1";
	TestMsg2.data = "Message2";
	TestMsg3.data = "Message3";	
	/***********************/
	
	TopicProxy<std_msgs::String, std_msgs::Int16> proxy;	
	std_msgs::Int16 talker1_msg;
	std_msgs::Int16 talker2_msg;
	std::unordered_map<std::string, std_msgs::String> mapmsg;
	
	proxy.Register("listener1", TopicType::AsSubscriber);
	proxy.Register("listener2", TopicType::AsSubscriber);
	proxy.Register("listener3", TopicType::AsSubscriber);

	proxy.Register("talker1", TopicType::AsPublisher);
	proxy.Register("talker2", TopicType::AsPublisher);

	talker1_msg.data = 1;
	talker2_msg.data = 2;

	ros::Rate loop_rate(10);	
	
	TimeValue elapsed;
	TimeValue ratecheck;

	Time::Tic(&elapsed);

	while(ros::ok()) {
	
		Time::Tic(&ratecheck);
		/*** TESTING SERVER ***/
		srvpub1.publish(TestMsg1);
		srvpub2.publish(TestMsg2);
		
		if(Time::Toc(&elapsed) < 5000) {
			srvpub3.publish(TestMsg3);
		}
		/***********************/

		proxy.GetMessages(mapmsg);


		if(proxy.HasUpdate("listener1"))
				printf("Message from listener1: %s\n", mapmsg.find("listener1")->second.data.c_str());

		if(proxy.HasUpdate("listener2"))
				printf("Message from listener2: %s\n", mapmsg.find("listener2")->second.data.c_str());

		if(proxy.HasUpdate("listener3"))
				printf("Message from listener3: %s\n", mapmsg.find("listener3")->second.data.c_str());


		proxy.Publish("talker1", talker1_msg);
		proxy.Publish("talker2", talker2_msg);

		ros::spinOnce();
		loop_rate.sleep();
		printf("Elapsed Time: %f - Rate check: %f\n", Time::Toc(&elapsed), Time::Toc(&ratecheck));
	}

	return 0;
}

#include <iostream>

#include "rossc_core/Client.hpp"
#include "rossc_core/Time.hpp"
#include "rossc_core/Endpoint.hpp"
#include "rossc_core/Socket.hpp"
#include "rossc_core/SocketProxy.hpp"

using namespace rossc::core;

class EventHandler : public SocketProxy {
	public:
		void HandleConnect(Socket* caller) { 
			Client *client = (Client*)caller;
			printf("[EventHandler] Client acquired local UDP socket: %s\n", 
					client->GetLocal().c_str());
			printf("[EventHandler] Client connected remote UDP socket: %s\n", 
					client->GetRemote().c_str());
		}
	
		void HandleDisconnect(Socket* caller) { 
			Client *client = (Client*)caller;
			printf("[EventHandler] Client disconnected & local UDP socket released: %s\n", 
					client->GetLocal().c_str());
		}
		
		void HandleRecv(Socket* caller) { 
			printf("[EventHandler] Client received a message\n");
		}
};

void RegisterAll(Client* client, EventHandler *handler) {
	Callback_Socket(client->iOnConnect,    handler, HandleConnect);
	Callback_Socket(client->iOnDisconnect, handler, HandleDisconnect);
	Callback_Socket(client->iOnRecv,   	 handler, HandleRecv);
}

int main(void) {
	
	Client* client = new Client();
	EventHandler* handler = new EventHandler();

	RegisterAll(client, handler);
	Endpoint peer("192.168.202.153", "5555");
	while(true) {
		while(client->Connect(peer.GetAddress()) == false) {
			printf("Waiting for connection...\n");
			Time::Sleep(1000);
		}

		for(int i = 0; i < 20; i++) {
			client->Send((const char*)"Hello there!");
			if(client->IsConnected() == false) {
				printf("Disconnected...\n");
				break;
			}
			Time::Sleep(1000);
		}
		client->Disconnect();
		Time::Sleep(2500);
	}
	
	client->Disconnect();
	delete client;

	return 0;
}

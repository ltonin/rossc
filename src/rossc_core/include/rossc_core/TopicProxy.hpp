#ifndef TOPICPROXY_HPP
#define TOPICPROXY_HPP

#include <string>
#include <unordered_map>
#include <functional>

#include <ros/ros.h>

#define DEFAULT_MSG_SIZE 1000

namespace rossc {
	namespace core {

typedef std::unordered_map<std::string, ros::Publisher> MapPublishers;
typedef std::unordered_map<std::string, ros::Subscriber> MapSubscribers;
typedef std::unordered_map<std::string, bool> StatusMessages;

template<class S>
using MapMessages = std::unordered_map<std::string, S>;

enum TopicType {
	AsSubscriber, 
	AsPublisher
};

template<class S, class P>
class TopicProxy {
	public:
		TopicProxy(void);
		virtual ~TopicProxy(void);
		
		void Publish(std::string topic, P msg);
		bool GetMessages(MapMessages<S>& messages);
		bool HasUpdate(std::string topic);
	public:

		bool Register(std::string topic, rossc::core::TopicType type, unsigned int size = DEFAULT_MSG_SIZE);

	private:
		bool register_publisher(std::string topic, unsigned int size);
		bool register_subscriber(std::string topic, unsigned int size);
		void sub_callback(const typename S::ConstPtr& msg, std::string topic);

	protected:
		ros::NodeHandle node_;
		MapPublishers   pubs_;
		MapSubscribers  subs_;
		MapMessages<S> recv_messages_;
		StatusMessages status_messages_;
};

template<class S, class P>
TopicProxy<S, P>::TopicProxy(void) {
}

template<class S, class P>
TopicProxy<S, P>::~TopicProxy(void) {
}

template<class S, class P>
bool TopicProxy<S, P>::Register(std::string topic, rossc::core::TopicType type, unsigned int size) {

	bool registered = false;

	switch(type) {
		case rossc::core::TopicType::AsSubscriber:
			
			if(registered = this->register_subscriber(topic, size)) 
				printf("[TopicProxy] - Subscription to topic '%s'. Listening...\n", topic.c_str());
			
			break;
		case rossc::core::TopicType::AsPublisher:

			if(registered = this->register_publisher(topic, size)) 
				printf("[TopicProxy] - Publisher advertise to topic '%s'. Ready to publish.\n", topic.c_str());

			break;
		default:
			break;
	}
	
	if (registered == false)
		printf("[TopicProxy] - Topic '%s' not inserted\n", topic.c_str());

}

template<class S, class P>
void TopicProxy<S, P>::Publish(std::string topic, P msg) {

	auto it = this->pubs_.find(topic);
	
	if(it == this->pubs_.end()) {
		printf("[TopicProxy] - Topic '%s' does not exist. Message is not published\n", topic.c_str());
	} else {
		it->second.publish(msg);
	}

}

template<class S, class P>
bool TopicProxy<S, P>::GetMessages(MapMessages<S>& messages) {
	bool retcode = false;

	if (this->recv_messages_.empty() == false) {
		messages.clear();
		messages = this->recv_messages_;
		retcode = true;
	}
	
	return retcode;
}

template<class S, class P>
bool TopicProxy<S, P>::HasUpdate(std::string topic) {
	bool updated = false;
	auto it = this->status_messages_.find(topic);

	if(it != this->status_messages_.end()) {
 		updated = it->second;
		it->second = false;
	}

	return updated;
}

template<class S, class P>
bool TopicProxy<S, P>::register_publisher(std::string topic, unsigned int size) {

	bool result;
	auto it = this->pubs_.find(topic);

	if(it != this->pubs_.end()) {
		printf("[TopicProxy] - Warning: publisher topic '%s' already exists\n", topic.c_str());
		result = false;
	} else {
		ros::Publisher pub = this->node_.template advertise<P>(topic, size);
		result = this->pubs_.insert(std::pair<std::string, ros::Publisher>(topic, pub)).second;
	}

	return result;
}

template<class S, class P>
bool TopicProxy<S, P>::register_subscriber(std::string topic, unsigned int size) {

	bool result;
	auto it = this->subs_.find(topic);
	
	if(it != this->subs_.end()) {
		printf("[TopicProxy] - Warning: subscriber topic '%s' already exists\n", topic.c_str());
		result = false;
	} else {
		const typename S::ConstPtr msg;
		ros::Subscriber sub = this->node_. template subscribe<S>(topic, size, boost::bind(&TopicProxy::sub_callback, this, _1, topic));
		result = this->subs_.insert(std::pair<std::string, ros::Subscriber>(topic, sub)).second;
	}

	return result;
}

template<class S, class P>
void TopicProxy<S, P>::sub_callback(const typename S::ConstPtr& msg, std::string topic) {
	this->recv_messages_[topic]   = *msg;
	this->status_messages_[topic] = true;
}

	}	
}

#endif

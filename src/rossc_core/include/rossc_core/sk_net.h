/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified by Luca Tonin - 2015 - University of Padova
*/

#ifndef SK_NET_H
#define SK_NET_H

#ifdef __cplusplus
extern "C" {
#endif 

#include "sk_types.h"


/*! \file sk_net.h 
 *	\brief Abstraction layer for TCP and UDP networking 
 *
 *	You should use the generic functions here defined unless you need to work at
 *	a lower level.
 */ 

/*! \brief Initialize the sk_host structure
 * 
 * Default values applied here.
 *
 * @param host The host
 */
void sk_init_host(sk_host* host);

/*! \brief Initialize the sk_socket structure
 *
 * Full customization
 *
 * @param sock The socket
 * @param bsize Buffer size
 * @param max_conns Maximum number of connections
 */
void sk_init_socket(sk_socket* sock, size_t bsize, unsigned int max_conns);

/*! \brief Open the socket
 * 
 * Setup the socket according to its role (server, client or endpoint) and its
 * protocol (TPC or UDP).
 *
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_open(sk_socket* sock);

/*! \brief Close the socket. 
 *
 * Reset the file descriptor and the socket role.
 *
 * @param sock The socket
 * @return -1 if socket is not initialized at TCP/UDP, 0 if successfull
 */
int sk_close(sk_socket* sock);

/*! \brief Frees the socket buffer
 *
 * The internal buffer is not freed by sk_close() to allow reuse of the socket
 *
 * @param sock The socket
 */
void sk_free(sk_socket* sock);

/*! 
 * \brief Check if sk_socket has a valid file descriptor
 *
 * @param sock The socket
 * @return 0 if not connected/bound, the file descriptor otherwise
 */
int sk_check(sk_socket* sock);

/*!  \brief Bind the socket
 * 
 * This method binds the socket to a port.
 *
 * @param sock The socket
 * @param port The IP address
 * @param port The port
 * @return 0 if successfull, -1 otherwise
 */
int sk_bind(sk_socket* sock, const char* ip, const char* port);

/*! \brief Start listening
 * 
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_listen(sk_socket* sock);

/*! \brief Accept a new client
 * 
 * @param sock The socket
 * @param endpoint The socket client, initialized as endpoint
 * @return 0 if successfull, -1 otherwise
 */
int sk_accept(sk_socket* sock, sk_socket* endpoint);

/*! \brief Connect to server
 * 
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_connect(sk_socket* sock, const char* host, const char* port);

/*! \brief Check if connected
 * 
 * @param sock The socket
 * @return 0 if connected, -1 otherwise
 */
int sk_connected(sk_socket* sock);

/*! \brief Receive in internal buffer
 * 
 * @param sock The socket
 * @return -1 if the connection is not valid, the number of received bytes.
 * otherwise
 */
ssize_t sk_recv(sk_socket* sock);

/*! \brief Receive in external buffer
 * 
 * @param sock The socket
 * @return -1 if the connection is not valid, the number of received bytes
 * otherwise
 */
ssize_t sk_recvb(sk_socket* sock, void* buffer, size_t bsize);

/*! \brief Send data
 * 
 * @param sock The socket
 * @return -1 if the connection is not valid, the number of sent bytes
 * otherwise
 */
ssize_t sk_send(sk_socket* sock, const char* buffer);

/*! \brief Send non-char data
 *
 * @param sock The socket
 * @return -1 if the connection is not valid, the number of sent bytes
 * otherwise
 */
ssize_t sk_sendb(sk_socket* sock, void* buffer, size_t bsize);

/*! \brief Setup the socket for async operation
 * 
 * This is needed to avoid blocking on receive
 *
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_set_nonblocking(sk_socket *socket, int value);

/*! \brief Get local host informations
 *
 * Set local ip and port in the sk_host structure
 *
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_gethost_local(sk_socket* sock, sk_host *host);

/*! \brief Get remote host informations
 * 
 * Set remote ip and port in the sk_host structure
 *
 * @param sock The socket
 * @return 0 if successfull, -1 otherwise
 */
int sk_gethost_remote(sk_socket* sock, sk_host *host);

void sk_getmaxbsize(sk_socket* sock);

#ifdef __cplusplus
}
#endif

#endif

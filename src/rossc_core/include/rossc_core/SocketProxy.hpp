#ifndef SOCKETPROXY_HPP 
#define SOCKETPROXY_HPP 

#include "rossc_core/Socket.hpp"
#include "rossc_core/Server.hpp" 

namespace rossc {
	namespace core {

/*! \brief Callback proxy for the Socket class
 */
class SocketProxy {
	public:
		virtual void HandleBind(Socket* caller);
		virtual void HandleRelease(Socket* caller);
		virtual void HandleListen(Socket* caller);
		
		virtual void HandleAccept(Socket* caller);
		virtual void HandleDrop(Socket* caller);
		
		virtual void HandleAcceptPeer(Socket* caller, Address addr);
		virtual void HandleDropPeer(Socket* caller, Address addr);
		
		virtual void HandleSend(Socket* caller);
		virtual void HandleRecv(Socket* caller, Streamer* stream);
		
		virtual void HandleSendPeer(Socket* caller, Address addr);
		virtual void HandleRecvPeer(Socket* caller, Address addr, Streamer* stream);

		virtual void HandleAccept(Socket* caller, Socket* ep);
		virtual void HandleDrop(Socket* caller, Socket* ep);
		
		virtual void HandleConnect(Socket* caller);
		virtual void HandleDisconnect(Socket* caller);
};

#define Callback_Socket(sender_event, receiver_ptr, method) \
	sender_event.Register((rossc::core::SocketProxy*)receiver_ptr, \
			&rossc::core::SocketProxy::method);

	}
}




#endif

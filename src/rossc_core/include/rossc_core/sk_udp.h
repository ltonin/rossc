/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
    Modified by Luca Tonin - 2015 - University of Padova
*/

#ifndef SK_UDP_H
#define SK_UDP_H

#ifdef __cplusplus
extern "C" {
#endif 

#include "sk_types.h"

/*! \file sk_udp.h 
 *	\brief Low-level UDP functionalities
 */ 

//! Initialize the sk_socket as a UDP server socket
void sk_udpserver(sk_socket* sock);

//! Initialize the sk_socket as a UDP client socket
void sk_udpclient(sk_socket* sock);

//! Receive as UDP on the internal buffer
ssize_t sk_recvudp(sk_socket* sock);

//! Receive as UDP
ssize_t sk_recvudpb(sk_socket* sock, void* buffer, size_t bsize);

//! Send as UDP
ssize_t sk_sendudp(sk_socket* sock, const void* buffer, size_t bsize);

#ifdef __cplusplus
}
#endif

#endif

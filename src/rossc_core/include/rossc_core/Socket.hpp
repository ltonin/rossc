#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <stdio.h>
#include <string.h>
#include <map>
#include "rossc_core/sk_names.h"
#include "rossc_core/sk_types.h"
#include "rossc_core/sk_net.h"
#include "rossc_core/Callback.hpp"
#include "rossc_core/Endpoint.hpp"
#include "rossc_core/Flags.hpp"
#include "rossc_core/NetworkTypes.hpp"
#include "rossc_core/Object.hpp"
#include "rossc_core/Semaphore.hpp"
#include "rossc_core/Streamer.hpp"
#include "rossc_core/ThreadSafe.hpp"

namespace rossc {
	namespace core {

/* Forward declaration */
class SocketProxy;


/*! \brief TCP/UDP socket
 * 
 * Implements the basic socket functionalities. Objects like Server and
 * Client heavily rely on this class. It implements all the methods to send
 * and receive ASCII and binary data.
 */
class Socket : public Object {
	public:
		Socket(size_t bsize);
		virtual ~Socket(void);
		virtual Address GetLocal(void);
		virtual Address GetRemote(void);
		long unsigned int GetSend(void);
		long unsigned int GetRecv(void);
		unsigned int GetPeers(void);
		int GetFID(void);
		virtual bool IsConnected(void);
		void Dump(void);
		static Address Lookup(std::string name);
	protected:
		virtual ssize_t Send(const char* message) = 0;
		virtual ssize_t Send(const std::string& message) = 0;
		virtual ssize_t Send(const void* message, size_t size) = 0;
		virtual ssize_t Send(sk_socket* peer, const void* message, size_t size); 
		virtual ssize_t Recv(sk_socket* peer); 
		virtual bool Open(int protocol) = 0;
		virtual bool Close(void);
		void AddSend(const unsigned int bytes);	
		void AddRecv(const unsigned int bytes);
		
		Streamer* GetStream(int fid);
		bool AddStream(int fid);
		bool RemStream(int fid);
		bool HasStream(int fid);
		
		sk_socket* GetPeer(int fid);
		bool AddPeer(sk_socket* peer);
		bool RemPeer(int fid);
		bool HasPeer(int fid);
		
		sk_socket* GetPeer(Address addr);
		Address GetAddress(int fid);

	protected:
		sk_socket* _socket;
		Semaphore _semsocket;
		StreamerMap _streams;
		SocketMap _peers;
	private:
		long unsigned int _bytesRecv;
		long unsigned int _bytesSend;
	public:
		const static int TCP = 0;
		const static int UDP = 1;
		Callback2<SocketProxy, Socket*, Streamer*> iOnRecv;
	protected:
		Callback1<SocketProxy, Socket*> iOnSend;
};
	}
}

#endif

#ifndef SERVER_HPP 
#define SERVER_HPP 

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>

#include "rossc_core/sk_net.h"
#include "rossc_core/sk_tcp.h"
#include "rossc_core/sk_udp.h"
#include "rossc_core/Socket.hpp"
#include "rossc_core/Thread.hpp"
#include "rossc_core/Endpoint.hpp"
#include "rossc_core/Time.hpp"

namespace rossc {
	namespace core {

/*! \brief TCP/UDP multiplexing server
 *
 * Server is a multiplexing TCP/UDP server.
 */
class Server : public Socket, protected Thread {
	public:
		Server(size_t bsize = CORE_1MB);
		virtual ~Server(void);
		bool Bind(Ip ip, Port port, int protocol = Socket::TCP);
		bool Bind(Address address, int protocol = Socket::TCP);
		bool Release(void);
		bool IsConnected(void);

		ssize_t Send(const char* message);
		ssize_t Send(const std::string& message);
		ssize_t Send(const void* message, size_t size);
		
		ssize_t Send(const char* message, Address addr);
		ssize_t Send(const std::string& message, Address addr);
		ssize_t Send(const void* message, size_t size, Address addr);
		
		ssize_t SendNot(const char* message, Address addr);
		ssize_t SendNot(const std::string& message, Address addr);
		ssize_t SendNot(const void* message, size_t size, Address addr);
	private:
		void Main(void);
		bool Open(int protocol);
		bool Listen(void);
		int Accept(void);
		void Drop(int fid);

	public:
		Callback1<SocketProxy, Socket*> iOnRelease;
		Callback1<SocketProxy, Socket*> iOnAccept;
		Callback1<SocketProxy, Socket*> iOnDrop;
		Callback2<SocketProxy, Socket*, Address> iOnAcceptPeer;
		Callback2<SocketProxy, Socket*, Address> iOnDropPeer;
		Callback3<SocketProxy, Socket*, Address, Streamer*> iOnRecvPeer;
	protected:
		Callback1<SocketProxy, Socket*> iOnBind;
		Callback1<SocketProxy, Socket*> iOnListen;
		Callback2<SocketProxy, Socket*, Address> iOnSendPeer;
};

	}
}
#endif

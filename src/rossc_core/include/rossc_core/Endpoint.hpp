#ifndef ENDPOINT_HPP
#define ENDPOINT_HPP

#include "rossc_core/NetworkTypes.hpp"
#include "rossc_core/Object.hpp"
#include "rossc_core/sk_net.h"
#include "rossc_core/sk_names.h"
#include <string>
#include <cstring>
#include <sstream>

namespace rossc {
	namespace core {

/*! \brief TCP/UDP endpoint
 *
 * This class represents a TCP/UDP connection endpoint, and can be used both by
 * a variety of classes such as Server and Client.
 */
class Endpoint : public Object {
	public:
		Endpoint(void);
		Endpoint(const Ip ip, const Port port);
		Endpoint(const Ip ip, const PortUInt port);
		Endpoint(const Address address);
		virtual ~Endpoint(void);

		virtual void Clear(void);

		virtual void Set(const sk_host *host, bool fixlocal = false);
		
		virtual void SetIp(const Ip ip);
		virtual Ip GetIp(void) const;

		virtual void SetPortUInt(const unsigned int port);
		virtual unsigned int GetPortUInt(void) const;
		
		virtual void SetPort(const Port port);
		virtual Port GetPort(void) const;
		
		virtual void SetAddress(const Address address);
		virtual Address GetAddress(void) const;
	private:
		virtual void MakeAddress(const Ip ip, const Port port);
		virtual void MakeAddress(const Ip ip, const unsigned int port);
		virtual void MakeAddress(const sk_host *host);
		virtual void DecomposeAddress(const Address address);
	public:
		const static Hostname HostnameUnset;
		const static Port PortUnset;
		const static PortUInt PortUintUnset;
		const static Ip IpUnset;
		const static Address AddressUnset;
	private:
		Ip _ip;
		Address _address;
		Port _port;
		unsigned int _iport;
};

	}
}
#endif

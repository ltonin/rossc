/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
    Modified by Luca Tonin - 2015 - University of Padova
*/

#ifndef SK_TCP_H
#define SK_TCP_H

#ifdef __cplusplus
extern "C" {
#endif 

#include "sk_types.h"

/*! \file sk_tcp.h 
 *	\brief Low-level TCP functionalities
 */ 

//! Initialize the sk_socket as a TCP server socket
void sk_tcpserver(sk_socket* sock);

//! Initialize the sk_socket as a TCP endpoint socket
void sk_tcpendpoint(sk_socket* sock);

//! Initialize the sk_socket as a TCP client socket
void sk_tcpclient(sk_socket* sock);

//! Receive as TCP on the internal buffer
ssize_t sk_recvtcp(sk_socket* sock);

//! Receive as TCP
ssize_t sk_recvtcpb(sk_socket* sock, void* buffer, size_t bsize);

//! Send as TCP
ssize_t sk_sendtcp(sk_socket* sock, const void* message, size_t bsize);

#ifdef __cplusplus
}
#endif

#endif

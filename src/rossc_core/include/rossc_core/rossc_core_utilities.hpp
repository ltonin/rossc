#ifndef ROSSC_CORE_UTILITIES_HPP
#define ROSSC_CORE_UTILITIES_HPP

#include <math.h>

namespace rossc {
	namespace core {

template<typename T>
T deg2rad(T deg) {
	return (deg*M_PI)/180.0f;
};

template<typename T>
T rad2deg(T rad) {
	return (rad*180.0f)/M_PI;
};


template<typename T>
T gaussian(T x, T s) {
	T arg;
	arg = - ( (pow(x, 2)) / (2.0f*pow(s, 2)));
	return exp(arg);
}

	}
}

#endif

#ifndef NETWORKTYPES_HPP
#define NETWORKTYPES_HPP

#include "rossc_core/sk_types.h"
#include <map>
#include <string>

namespace rossc {
	namespace core {

class Streamer;

typedef std::string Ip;
typedef std::string Port;
typedef unsigned int PortUInt;
typedef std::string Address;
typedef std::string Hostname;

typedef std::map<int, Streamer*> StreamerMap;
typedef std::map<int, Streamer*>::iterator StreamerMapIt;
typedef std::map<int, Streamer*>::const_iterator StreamerMapCIt;

typedef std::map<int, sk_socket*> SocketMap;
typedef std::map<int, sk_socket*>::iterator SocketMapIt;
typedef std::map<int, sk_socket*>::const_iterator SocketMapCIt;

	}
}

#endif

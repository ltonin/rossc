#ifndef THREADRUNNER_HPP
#define THREADRUNNER_HPP

#include "Thread.hpp"

namespace rossc {
	namespace core {


void* Threadrunner(void* implementation);

	}
}

#endif

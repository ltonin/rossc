#ifndef STREAMER_HPP
#define STREAMER_HPP

#include "rossc_core/Object.hpp"
#include "rossc_core/Semaphore.hpp"
#include <string>
#include <iostream>

namespace rossc {
	namespace core {

typedef unsigned int StreamerDirection;

/*! \brief Data streams handler
 *
 * This class is designed to allow an easy handling of data streams.
 * It allows to verify whether a certain message is present and to extract it
 * automatically. 
 */
class Streamer : public Object {
	public:
		Streamer(void);
		virtual ~Streamer(void);

		virtual void Append(std::string buffer);
		virtual void Append(const char* buffer, size_t bsize);
		virtual bool Extract(std::string* buffer, std::string hdr, 
				std::string trl, StreamerDirection direction = Streamer::Forward);
		virtual bool Extract(std::string* buffer);
		virtual bool Has(std::string hdr, std::string trl, 
				StreamerDirection direction = Streamer::Forward);
		virtual int Count(std::string hdr);
		virtual void Dump(void);
		virtual int Size(void);
		virtual void Clear(void);
	private:
		virtual bool ImplHas(std::string hdr, std::string trl, 
				StreamerDirection direction);

	private:
		std::string _stream;
		Semaphore _mtxstream;

	public:
		static const StreamerDirection Forward = 0;
		static const StreamerDirection Reverse = 1;
};

	}
}
#endif

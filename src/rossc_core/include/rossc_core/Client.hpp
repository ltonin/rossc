#ifndef CLIENT_HPP 
#define CLIENT_HPP 


#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>

#include "rossc_core/sk_net.h"
#include "rossc_core/sk_tcp.h"
#include "rossc_core/sk_udp.h"
#include "rossc_core/Endpoint.hpp"
#include "rossc_core/Time.hpp"
#include "rossc_core/Socket.hpp"
#include "rossc_core/Thread.hpp"

namespace rossc {
	namespace core {

/*! \brief TCP/UDP client
 *
 * Client is a threaded asynchronous TCP/UDP client.
 */
class Client : public Socket, protected Thread {
	public:
		Client(size_t bsize = CORE_1MB);
		virtual ~Client(void);
		bool Connect(Address address, int protocol = Socket::TCP);
		bool Disconnect(void);
		ssize_t Send(const char* message);
		ssize_t Send(const std::string& message);
		ssize_t Send(const void* message, size_t size);

	private:
		void Main(void);
		bool Open(int protocol);

	public:
		Callback1<SocketProxy, Socket*> iOnConnect;
		Callback1<SocketProxy, Socket*> iOnDisconnect;
};

	}
}

#endif

#ifndef CLIENT_CPP 
#define CLIENT_CPP 

#include "rossc_core/Client.hpp" 

namespace rossc {
	namespace core {

Client::Client(size_t bsize) : Socket(bsize) {
}

Client::~Client(void) {
	//this->Disconnect();
}
		
bool Client::Connect(Address address, int protocol) {
	Socket::_semsocket.Wait();
	if(Thread::IsRunning() == true) {
		std::cout<<"Client thread already running"<<std::endl;
		return true;
	}
		
	Socket::Close();
	if(this->Open(protocol) == false) {
		std::cerr<<"Cannot open socket"<<std::endl;
		Socket::_semsocket.Post();
		return false;
	}
	
	Endpoint cache(address);
	
	int status = sk_connect(Socket::_socket, 
			cache.GetIp().c_str(), cache.GetPort().c_str());
	if(status != 0) {
		Socket::_semsocket.Post();
		std::cerr<<"Cannot connect socket"<<std::endl;
		return false;
	}
	sk_set_nonblocking(Socket::_socket, 1);
	Socket::AddStream(Socket::_socket->fd);
	Socket::AddPeer(Socket::_socket);
	
	Endpoint local, remote;
	local.Set(&Socket::_socket->local);
	remote.Set(&Socket::_socket->remote);
	Socket::_semsocket.Post();

	Thread::Start();
	
	this->iOnConnect.Execute(this);
	return true;
}

bool Client::Disconnect(void) {
	if(this->IsConnected() == false) {
		printf("Socket not connected");
		return false;
	}

	if(Thread::IsRunning() == true) {
		Thread::Stop();
		Thread::Join();
	}
	
	Socket::_semsocket.Wait();
	if(this->Close() == false) {
		Socket::_semsocket.Post();
		return false;
	}
	Socket::RemStream(Socket::_socket->fd);
	Socket::_semsocket.Post();
	
	this->iOnDisconnect.Execute(this);
	return true;
}

ssize_t Client::Send(const char* message) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, (void*)message,
			strlen(message));
	Socket::_semsocket.Post();
	
	return bytes;
}

ssize_t Client::Send(const std::string& message) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, (void*)message.c_str(),
			message.size());
	Socket::_semsocket.Post();
	
	return bytes;
}
		
ssize_t Client::Send(const void* message, size_t size) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, message, size);
	Socket::_semsocket.Post();

	return bytes;
}
		
void Client::Main(void) {
	if(!Thread::IsRunning())
		return;
	
	struct timeval tv;
	fd_set readfds;
	FD_ZERO(&readfds);

	int status;
	bool received;
	Streamer* stream = Socket::GetStream(Socket::_socket->fd);
	while(Thread::IsRunning()) {
		FD_SET(Socket::_socket->fd, &readfds);
		tv.tv_sec = CORE_ASIO_SEC;
		tv.tv_usec = CORE_ASIO_USEC;
		
		received = false;

		status = select(Socket::_socket->fd + 1, &readfds, NULL, NULL, &tv);
		if(status == -1) {
			std::cerr<<"Async I/O error: " << strerror(status)<<std::endl;
			Thread::Stop();
		} else if(status > 0) {
			if(FD_ISSET(Socket::_socket->fd, &readfds)) {
				Socket::_semsocket.Wait();
				if(Socket::Recv(Socket::_socket) == TR_BYTES_ERROR) {
					Thread::Stop();
				} else {
					received = true;
				}
				Socket::_semsocket.Post();
			}
		}
		if(received) 
			Socket::iOnRecv.Execute(this, stream);
	}
	FD_ZERO(&readfds);
	
	this->Disconnect();
	Socket::_semsocket.Wait();
	Socket::RemStream(Socket::_socket->fd);
	Socket::_semsocket.Post();
	this->iOnDisconnect.Execute(this);
}

bool Client::Open(int protocol) {
	int status = 0;
	switch(protocol) {
		case Socket::UDP:
			sk_udpclient(Socket::_socket);
			break;
		case Socket::TCP:
		default:
			sk_tcpclient(Socket::_socket);
			break;
	}
	status = sk_open(Socket::_socket);
	
	if(status != 0)	{
		std::cerr<<"Cannot open socket as " << 
				(protocol == Socket::UDP ? "UDP" : "TCP")<<std::endl;
	}
	return(status == 0);
}

		}
}	
#endif

#ifndef SOCKETPROXY_CPP 
#define SOCKETPROXY_CPP 

#include "rossc_core/SocketProxy.hpp" 

namespace rossc {
	namespace core {

		void SocketProxy::HandleBind(Socket* caller) {
	std::cout<<"Socket " << caller->GetFID() << " bound"<<std::endl;
}

void SocketProxy::HandleRelease(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " released"<<std::endl;
}

void SocketProxy::HandleListen(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " listening"<<std::endl;
}

void SocketProxy::HandleAccept(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " accepted"<<std::endl;
}

void SocketProxy::HandleDrop(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " dropped"<<std::endl;
}

void SocketProxy::HandleAcceptPeer(Socket* caller, Address addr) { 
	std::cout<<"Socket " << caller->GetFID() << " accepted " << addr<<std::endl;
}

void SocketProxy::HandleDropPeer(Socket* caller, Address addr) { 
	std::cout<<"Socket " << caller->GetFID() << " dropped " << addr<<std::endl;
}

void SocketProxy::HandleSend(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " sent data"<<std::endl;
}

void SocketProxy::HandleRecv(Socket* caller, Streamer* stream) { 
	std::cout<<"Socket " << caller->GetFID() << " received data"<<std::endl;
}

void SocketProxy::HandleSendPeer(Socket* caller, Address addr) { 
	std::cout<<"Socket " << caller->GetFID() << " sent to " << addr<<std::endl;
}

void SocketProxy::HandleRecvPeer(Socket* caller, Address addr, 
		Streamer* stream) { 
	std::cout<<"Socket " << caller->GetFID() << " received from " << addr<<std::endl;
}

void SocketProxy::HandleAccept(Socket* caller, Socket* ep) { 
	std::cout<<"Socket " << caller->GetFID() << " accepted " << ep->GetFID()<<std::endl;
}

void SocketProxy::HandleDrop(Socket* caller, Socket* ep) { 
	std::cout<<"Socket " << caller->GetFID() << " dropped " << ep->GetFID()<<std::endl;
}

void SocketProxy::HandleConnect(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " connected"<<std::endl;
}

void SocketProxy::HandleDisconnect(Socket* caller) { 
	std::cout<<"Socket " << caller->GetFID() << " disconnected"<<std::endl;
}

	}
}

#endif

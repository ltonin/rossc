#ifndef THREADRUNNER_CPP
#define THREADRUNNER_CPP

#include "rossc_core/Threadrunner.hpp"

namespace rossc {
	namespace core {


void* Threadrunner(void* implementation) {
	rossc::core::Thread *runner = (rossc::core::Thread *)implementation;
	runner->Main();
	
	return NULL;
}

	}
}

#endif

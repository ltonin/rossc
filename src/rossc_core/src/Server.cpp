#ifndef SERVER_CPP 
#define SERVER_CPP 

#include "rossc_core/Server.hpp" 

namespace rossc {
	namespace core {

Server::Server(size_t bsize) : Socket(bsize) {
}

Server::~Server(void) {
}
		
bool Server::Bind(Ip ip, Port port, int protocol) {
	Socket::_semsocket.Wait();
	if(Thread::IsRunning() == true) {
		std::cout<<"Client thread already running"<<std::endl;
		return true;
	}
	
	Socket::Close();
	if(this->Open(protocol) == false) {
		std::cerr<<"Cannot open socket"<<std::endl;
		Socket::_semsocket.Post();
		return false;
	}
	
	int statusb = 0;
	statusb = sk_bind(Socket::_socket, ip.c_str(), port.c_str());
	sk_set_nonblocking(Socket::_socket, 1);
	Socket::AddStream(Socket::_socket->fd);
	Socket::AddPeer(Socket::_socket);
	
	if(statusb != 0) {
		Socket::_semsocket.Post();
		std::cerr<<"Cannot bind socket"<<std::endl;
		return false;
	}
	
	Endpoint local;
	local.Set(&Socket::_socket->local);
	this->iOnBind.Execute(this);
	
	if(this->Listen() == false)
		return false;
	Socket::_semsocket.Post();
	
	Thread::Start();
	return true;
}
		
bool Server::Bind(Address address, int protocol) {
	Endpoint ep(address);
	return this->Bind(ep.GetIp(), ep.GetPort());
}


bool Server::Release(void) {
	if(this->IsConnected() == false) {
		std::cout<<"Socket not connected"<<std::endl;
		return false;
	}

	if(Thread::IsRunning()) {
		Thread::Stop();
		Thread::Join();
	}

	Socket::_semsocket.Wait();
	if(this->Close() == false) {
		Socket::_semsocket.Post();
		return false;
	}
	Socket::RemStream(Socket::_socket->fd);
	Socket::_semsocket.Post();
	
	this->iOnRelease.Execute(this);
	return true;
}
		
ssize_t Server::Send(const char* message) {
	return this->Send((void*)message, strlen(message));
}

ssize_t Server::Send(const std::string& message) {
	return this->Send((void*)message.c_str(), message.size());
}

ssize_t Server::Send(const void* message, size_t size) {
	Socket::_semsocket.Wait();
	SocketMapIt pit;
	ssize_t sendtot = 0;
	for(pit = this->_peers.begin(); pit != this->_peers.end(); pit++) {
		sendtot += Socket::Send(pit->second, message, size);
	}
	Socket::_semsocket.Post();
	
	return sendtot;
}
		
ssize_t Server::Send(const char* message, Address addr) {
	return this->Send((void*)message, strlen(message), addr);
}

ssize_t Server::Send(const std::string& message, Address addr) {
	return this->Send((void*)message.c_str(), message.size(), addr);
}

ssize_t Server::Send(const void* message, size_t size, Address addr) {
	Socket::_semsocket.Wait();
	sk_socket* peer = GetPeer(addr);
	if(peer == NULL) {
		std::cerr<<"No peer found for: " << addr <<std::endl;
		Socket::_semsocket.Post();
		return TR_BYTES_NONE;
	}

	ssize_t bytes = Socket::Send(peer, message, size); 
	Socket::_semsocket.Post();

	return bytes;
}
		
ssize_t Server::SendNot(const char* message, Address addr) {
	return this->SendNot((void*)message, strlen(message), addr);
}

ssize_t Server::SendNot(const std::string& message, Address addr) {
	return this->SendNot((void*)message.c_str(), message.size(), addr);
}

ssize_t Server::SendNot(const void* message, size_t size, Address addr) {
	Socket::_semsocket.Wait();
	SocketMapIt pit;
	sk_socket* peer = GetPeer(addr);
	if(peer == NULL) {
		std::cerr<<"No peer found for: " << addr<<std::endl;
		Socket::_semsocket.Post();
		return TR_BYTES_NONE;
	}

	ssize_t sendtot = 0;
	for(pit = this->_peers.begin(); pit != this->_peers.end(); pit++) {
		if(pit->second->fd != peer->fd)
			sendtot += Socket::Send(pit->second, message, size);
	}
	Socket::_semsocket.Post();
	
	return sendtot;
}

void Server::Main(void) {
	if(!Thread::IsRunning())
		return;
	
	struct timeval tv;
	fd_set masterfds, readfds;
	FD_ZERO(&masterfds);
	
	int fdmax = Socket::_socket->fd,
		fdlis = Socket::_socket->fd,
		fdnew = 0;
	sk_socket* peer = NULL;
	FD_SET(fdlis, &masterfds);

	int status;
	while(Thread::IsRunning()) {
		tv.tv_sec = CORE_ASIO_SEC;
		tv.tv_usec = CORE_ASIO_USEC;

		readfds = masterfds;
		status = select(fdmax + 1, &readfds, NULL, NULL, &tv); 
		if(status == -1) {
			std::cerr<<"Async I/O error: " << strerror(status)<<std::endl;
		}
		
		int fida, fidr, fidd;
		Address address;
		Streamer* stream = NULL;
		for(int i = 0; i <= fdmax; i++) {
			if(FD_ISSET(i, &readfds) <= 0)
				continue;

			fida = 0;
			fidd = 0;
			fidr = 0;

			Socket::_semsocket.Wait();
			if(i == fdlis) {
				fdnew = this->Accept();
				if(fdnew > 0) {
					if(fdnew > fdmax)
						fdmax = fdnew;
					FD_SET(fdnew, &masterfds);
					fida = fdnew;
					address = Socket::GetAddress(fida);
				}
			} else {
				if(Socket::HasPeer(i) == false) {
					std::cerr<<"ASIO peer table corrupted"<<std::endl;
					Socket::_semsocket.Post();
					Thread::Stop();
					break;
				}
				
				peer = GetPeer(i);
				if(Socket::Recv(peer) == TR_BYTES_ERROR) {
					fidd = i;
					address = Socket::GetAddress(fidd);
					FD_CLR(i, &masterfds);
					this->Drop(i);
				} else {
					fidr = i;
					address = Socket::GetAddress(fidr);
					stream = Socket::GetStream(fidr);
				}
			}
			Socket::_semsocket.Post();

			if(fida > 0) {
				this->iOnAccept.Execute(this);
				this->iOnAcceptPeer.Execute(this, address);
			}
			if(fidr > 0) {
				Socket::iOnRecv.Execute(this, stream);
				this->iOnRecvPeer.Execute(this, address, stream);
			}
			if(fidd > 0) {
				this->iOnDrop.Execute(this);
				this->iOnDropPeer.Execute(this, address);
			}
		}
	}
	FD_ZERO(&readfds);
	FD_ZERO(&masterfds);
	this->Release();
}

void Server::Drop(int fid) {
	sk_socket* peer = Socket::GetPeer(fid);
	sk_close(peer);
	sk_free(peer);
	Socket::RemStream(fid);
	Socket::RemPeer(fid);
}
		
bool Server::Open(int protocol) {
	switch(protocol) {
		case Socket::UDP:
			sk_udpserver(Socket::_socket);
			break;
		case Socket::TCP:
		default:
			sk_tcpserver(Socket::_socket);
			break;
	}
	int status = sk_open(Socket::_socket);
	
	if(status != 0)	{
		std::cerr<<"Cannot open socket as " << 
				(protocol == Socket::UDP ? "UDP" : "TCP")<<std::endl;
	}
	return(status == 0);
}
		
bool Server::Listen(void) {
	if(sk_listen(Socket::_socket) != 0) {
		std::cerr<<"Cannot listen on socket"<<std::endl;
		return false;
	}
	
	this->iOnListen.Execute(this);

	return true;
}

int Server::Accept(void) {
	sk_socket* peer = new sk_socket;
	sk_init_socket(peer, CORE_1MB, 1);
	sk_tcpendpoint(peer);
	
	int fid = sk_accept(Socket::_socket, peer);
	if(fid <= 0)
		return fid;

	sk_set_nonblocking(peer, 1);
	Socket::AddPeer(peer);
	Socket::AddStream(fid);
	
	return fid;
}
		
bool Server::IsConnected(void) {
	return Thread::IsRunning();
}

	}
}
#endif

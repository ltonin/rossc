/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified by Luca Tonin - 2015 - University of Padova
*/

 
#include "rossc_core/sk_net.h"
#include "rossc_core/sk_tcp.h"
#include "rossc_core/sk_udp.h"
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int sk_flags(sk_socket* sock) {
	int opts = fcntl(sock->fd, F_GETFL);
	if(opts < 0)
		return 1;
#ifdef O_CLOEXEC
	opts = (opts | O_CLOEXEC);
#endif
	if(fcntl(sock->fd, F_SETFL, opts) < 0)
		return 1;
	return 0;
}

void sk_init_host(sk_host* host) {
	memset(host->address, 0, INET_ADDRSTRLEN + 1);
	host->port = 0;
	host->valid = TR_HOST_NOTVALID;
}

void sk_init_socket(sk_socket* sock, size_t bsize, unsigned int max_conns) {
	memset(sock, 0, sizeof(sock));
	sock->fd = TR_SOCKET_UNDEF;
	sock->type = TR_TYPE_UNDEF;
	sock->protocol = TR_PROTO_UNDEF;
	
	sock->max_conns = max_conns;
	
	sock->bsize = bsize;
	sock->buffer = malloc(bsize * sizeof(char));
	memset(sock->buffer, 0, sock->bsize);
	
	memset(&(sock->address), 0, sizeof(sock->address));
	memset(&(sock->info_results), 0, sizeof(sock->info_results));
	memset(&(sock->address_endpoint), 0, sizeof(sock->address_endpoint));

	sk_init_host(&(sock->local));
	sk_init_host(&(sock->remote));

	sock->maxbsize = 0;
}

int sk_open(sk_socket* sock) {
	switch(sock->protocol) {
		case TR_PROTO_TCP:
			sock->info_results.ai_family = AF_UNSPEC;
			sock->info_results.ai_socktype = SOCK_STREAM;
			if(sock->type != TR_TYPE_CLIENT)
				sock->info_results.ai_flags = AI_PASSIVE;
			break;
		case TR_PROTO_UDP:
			sock->info_results.ai_family = AF_UNSPEC;
			sock->info_results.ai_socktype = SOCK_DGRAM;
			if(sock->type != TR_TYPE_CLIENT) 
				sock->info_results.ai_flags = AI_PASSIVE;
			break;
		default:
			return -1;
	}
	return 0;
}

int sk_close(sk_socket* sock) {
	int status = 0;
	if(sock->fd) {
		status = close(sock->fd);
		sock->fd = TR_SOCKET_UNDEF;
		sock->type = TR_TYPE_UNDEF;
	}
	return status;
}

void sk_free(sk_socket* sock) {
	if(sock->buffer != 0) {
		free(sock->buffer);
		sock->buffer = 0;
	}
}

int sk_check(sk_socket* sock) {
	return sock->fd;
}

int sk_bind(sk_socket* sock, const char* ip, const char* port) {
	int retopt = 0;
	int bndret = 0;
	struct addrinfo *ai; 
	int opt = 1;

	if(getaddrinfo(ip, port, &(sock->info_results), &ai) != 0)
		return -1;

	for(sock->info = ai; sock->info != NULL; sock->info = sock->info->ai_next) {
		sock->fd = socket(sock->info->ai_family, 
				sock->info->ai_socktype,
				sock->info->ai_protocol);
		if(sock->fd == -1) 
			continue;

		if(sock->protocol == TR_PROTO_TCP) {
			retopt = setsockopt(sock->fd, 
					SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));
			if(retopt == -1) 
				return -1;
		}

		bndret = bind(sock->fd, sock->info->ai_addr, sock->info->ai_addrlen); 
		sk_flags(sock);
		if(bndret == -1) {
			close(sock->fd);
			continue;
		}
		break;
	}
	freeaddrinfo(ai);

	// Fill local host structure
	sk_gethost_local(sock, &(sock->local));

	return bndret;
}

int sk_listen(sk_socket* sock) {
	if(!sk_check(sock))
		return -1;
	
	return listen(sock->fd, sock->max_conns);
}

int sk_accept(sk_socket* sock, sk_socket* endpoint) {
	if(sock->protocol != TR_PROTO_TCP) 
		return -3;

	unsigned int addrlen = sizeof(endpoint->address);
	endpoint->fd = accept(sock->fd, (struct sockaddr*)&sock->address_endpoint,
			&addrlen);
	sk_flags(endpoint);
	endpoint->type = TR_TYPE_ENDPOINT;

	// Fill local and remote host structures
	sk_gethost_local(endpoint, &(endpoint->local));
	sk_gethost_remote(endpoint, &(endpoint->remote));
	sk_getmaxbsize(endpoint);
	
	return endpoint->fd;
}

int sk_connect(sk_socket* sock, const char* host, const char* port) {
	struct addrinfo *ai;
	int conopt = 0;

	if(getaddrinfo(host, port, &(sock->info_results), &ai) != 0)
		return -1;
	
	for(sock->info = ai; sock->info != NULL; sock->info = sock->info->ai_next) {
		sock->fd = socket(sock->info->ai_family,
				sock->info->ai_socktype,
				sock->info->ai_protocol);
		if (sock->fd == -1) 
			continue;
		sk_flags(sock);

		if(sock->protocol == TR_PROTO_TCP) {
			conopt = connect(sock->fd, 
					sock->info->ai_addr,
					sock->info->ai_addrlen);
			if (conopt == -1) {
				close(sock->fd);
				continue;
			}
		}
		break;
	}

	sk_getmaxbsize(sock);

	//TODO check res here
	sk_gethost_local(sock, &(sock->local));
	sk_gethost_remote(sock, &(sock->remote));

	return conopt;
}

int sk_connected(sk_socket* sock) {
	return (int)sk_send(sock, "\0");
}

ssize_t sk_recv(sk_socket* sock) {
	switch(sock->protocol) {
		case TR_PROTO_TCP:
			return sk_recvtcp(sock);
			break;
		case TR_PROTO_UDP:
			return sk_recvudp(sock);
			break;
		default:
			return -2;
	}
}

ssize_t sk_recvb(sk_socket* sock, void* buffer, size_t bsize) {
	switch(sock->protocol) {
		case TR_PROTO_TCP:
			return sk_recvtcpb(sock, buffer, bsize);
			break;
		case TR_PROTO_UDP:
			return sk_recvudpb(sock, buffer, bsize);
			break;
		default:
			return -2;
	}
}

ssize_t sk_send(sk_socket* sock, const char* buffer) {
	switch(sock->protocol) {
		case TR_PROTO_TCP:
			return sk_sendtcp(sock, (void*)buffer, strlen(buffer));
			break;
		case TR_PROTO_UDP:
			return sk_sendudp(sock, (void*)buffer, strlen(buffer));
		default:
			return -2;
	}
}

ssize_t sk_sendb(sk_socket* sock, void* buffer, size_t bsize) {
	switch(sock->protocol) {
		case TR_PROTO_TCP:
			return sk_sendtcp(sock, buffer, bsize);
			break;
		case TR_PROTO_UDP:
			return sk_sendudp(sock, buffer, bsize);
		default:
			return -2;
	}
}

int sk_set_nonblocking(sk_socket *socket, int value) {
	int oldflags = fcntl(socket->fd, F_GETFL, 0);
	if (oldflags < 0)
		return oldflags;

	if (value != 0)
		oldflags |= O_NONBLOCK;
	else
		oldflags &= ~O_NONBLOCK;
	return fcntl(socket->fd, F_SETFL, oldflags);
}

int sk_gethost_local(sk_socket* sock, sk_host *host) {
	const char* status = NULL;
	struct sockaddr addr;
	int addrlen = sizeof(addr);

	getsockname(sock->fd, 
			(struct sockaddr*)&addr, (socklen_t*)&addrlen);
	struct sockaddr_in *addr_ptr = (struct sockaddr_in*)&addr;
	
	host->port = ntohs(addr_ptr->sin_port);
	status = inet_ntop(AF_INET, 
			&(addr_ptr->sin_addr.s_addr), host->address, (socklen_t)addrlen);

	if(status == NULL)
		return -1;

	host->valid = TR_HOST_VALID;
	return 0;
}

int sk_gethost_remote(sk_socket* sock, sk_host *host) {
	const char* status = NULL;
	struct sockaddr addr;
	int addrlen = sizeof(addr);
	
	getpeername(sock->fd, 
			(struct sockaddr*)&addr, (socklen_t*)&addrlen);
	struct sockaddr_in *addr_ptr = (struct sockaddr_in*)&addr;
	
	host->port = ntohs(addr_ptr->sin_port);
	status = inet_ntop(AF_INET, 
			&(addr_ptr->sin_addr.s_addr), host->address, (socklen_t)addrlen);

	if(status == NULL)
		return -1;
	host->valid = TR_HOST_VALID;
	return 0;
}

void sk_getmaxbsize(sk_socket* sock) {
	socklen_t optlen = sizeof(sock->maxbsize);
	if(getsockopt(sock->fd, SOL_SOCKET, SO_SNDBUF, &sock->maxbsize, &optlen) < 0) {
		sock->maxbsize = 0;
	}
}

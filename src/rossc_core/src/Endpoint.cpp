#ifndef ENDPOINT_CPP
#define ENDPOINT_CPP

#include "rossc_core/Endpoint.hpp"

namespace rossc {
	namespace core {

const Hostname Endpoint::HostnameUnset = "none";
const Port Endpoint::PortUnset = "0";
const Ip Endpoint::IpUnset = "0.0.0.0";
const Address Endpoint::AddressUnset = "0.0.0.0:0";
const PortUInt Endpoint::PortUintUnset = 0;

Endpoint::Endpoint(void) {
}

Endpoint::Endpoint(const Ip ip, const Port port) {
	this->SetIp(ip);
	this->SetPort(port);
}

Endpoint::Endpoint(const Ip ip, const unsigned int port) {
	this->SetIp(ip);
	this->SetPortUInt(port);
}
		
Endpoint::Endpoint(const Address address) {
	this->SetAddress(address);
}

Endpoint::~Endpoint(void) {
}

void Endpoint::Clear(void) {
	this->_ip = Endpoint::IpUnset;
	this->_port = Endpoint::PortUnset;
	this->_iport = Endpoint::PortUintUnset;
	this->_address = Endpoint::AddressUnset;
}

void Endpoint::SetIp(const Ip ip) {
	if(ip.size() > sk_getaddrlen())
		throw("Address exceeds predefined size");
	this->_ip = ip;
	this->MakeAddress(this->_ip, this->_iport);
}

void Endpoint::SetPortUInt(const unsigned int port) {
	this->_iport = port;
	this->MakeAddress(this->_ip, this->_iport);
	// TODO create an ultimate way to perform conversions
	char buffer[5];
	sprintf(buffer, "%d", port);
	this->_port.assign(buffer);
}

Ip Endpoint::GetIp(void) const {
	return this->_ip;
}

unsigned int Endpoint::GetPortUInt(void) const {
	return this->_iport;
}

void Endpoint::SetPort(const Port port) {
	this->_port = port;
	this->MakeAddress(this->_ip, this->_port);
	// TODO create an ultimate way to perform conversions
	sscanf(port.c_str(), "%d", &this->_iport);
}

Port Endpoint::GetPort(void) const {
	return this->_port;
}

void Endpoint::Set(const sk_host *host, bool fixlocal) {
	if(strcmp(host->address, "0.0.0.0") == 0 && fixlocal)
		this->_ip.assign("127.0.0.1");
	else
		this->_ip.assign(host->address);
	this->_iport = host->port;

	this->MakeAddress(this->_ip, this->_iport);
}
		
void Endpoint::SetAddress(const Address address) {
	this->_address = address;
	Endpoint::DecomposeAddress(this->_address);
}

Address Endpoint::GetAddress(void) const {
	return this->_address;
}

void Endpoint::MakeAddress(const Ip ip, const Port port) {
	this->_address.clear();
	this->_address.append(ip);
	this->_address.append(":");
	this->_address.append(port);
}

void Endpoint::MakeAddress(const Ip ip, const unsigned int port) {
	std::stringstream stream;
	if(port == 0 || ip.empty() == true) {
		this->_address.clear();
	} else {
		stream << ip;
		stream << ":";
		stream << port;
		this->_address = stream.str();
	}
}
		
void Endpoint::MakeAddress(const sk_host *host) {
	Ip ip(host->address);
	this->MakeAddress(ip, host->port);
}
		
void Endpoint::DecomposeAddress(const Address address) {
	char cip[32];
	if(address.find(":") != std::string::npos) {
		sscanf(address.c_str(), "%[^':']:%u", cip, &this->_iport);
		this->_ip.assign(cip);
	} else {
		sscanf(address.c_str(), "%u", &this->_iport);
	}

	std::stringstream stream;
	stream << this->_iport;
	this->_port.assign(stream.str());
}
	}
}
#endif

/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified by Luca Tonin - 2015 - University of Padova
*/


#include "rossc_core/sk_udp.h"
#include "rossc_core/sk_net.h"
#include <string.h>

void sk_udpserver(sk_socket* sock) {
	sock->protocol = TR_PROTO_UDP;
	sock->type = TR_TYPE_SERVER;
}

void sk_udpclient(sk_socket* sock) {
	sock->protocol = TR_PROTO_UDP;
	sock->type = TR_TYPE_CLIENT;
}

ssize_t sk_recvudp(sk_socket* sock) {
	return sk_recvudpb(sock, sock->buffer, sock->bsize);
}

ssize_t sk_recvudpb(sk_socket* sock, void* buffer, size_t bsize) {
	if(sock->protocol != TR_PROTO_UDP)
		return TR_PROTO_NOTSUPPORTED;

	int addr_len = sizeof(sock->address_endpoint);
	int status = recvfrom(sock->fd, buffer, bsize, 0,
			(struct sockaddr *)&sock->address_endpoint, (socklen_t*)&addr_len);
	
	sk_gethost_remote(sock, &(sock->remote));
	return status;
}

ssize_t sk_sendudp(sk_socket* sock, const void* buffer, size_t bsize) {
	if(sock->protocol != TR_PROTO_UDP)
		return TR_PROTO_NOTSUPPORTED;

	return sendto(sock->fd, buffer, bsize, 0, 
			sock->info->ai_addr, sock->info->ai_addrlen);
}

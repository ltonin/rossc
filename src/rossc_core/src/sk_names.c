/*
    Copyright (C) 2009-2011  EPFL (Ecole Polytechnique Fédérale de Lausanne)
    Michele Tavella <michele.tavella@epfl.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Modified by Luca Tonin - 2015 - University of Padova
*/

#include "rossc_core/sk_names.h"
#include "rossc_core/sk_types.h"
#include <netinet/in.h>
#include <string.h>

unsigned int sk_getaddrlen(void) { 
	return 2*INET_ADDRSTRLEN + 1;
}

int sk_resolve(const char* hostname, char* address) {
	struct hostent *host;
	struct in_addr h_addr;
	
	host = gethostbyname(hostname);
	if(host == NULL)
		return 1;

	h_addr.s_addr = *((unsigned long *)host->h_addr_list[0]);
	char* cache = inet_ntoa(h_addr); 
	strncpy(address, cache, sk_getaddrlen()-1);
	return 0;
}

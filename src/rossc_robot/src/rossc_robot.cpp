#include <ros/ros.h>
#include "std_msgs/Int16.h"
#include <geometry_msgs/Twist.h>
#include <kobuki_msgs/BumperEvent.h>

#include "rossc_core/rossc_core.h"
#include "rossc_actor/SystemControl.hpp"

#define BACKWARD_TIME 	2000.0f 	// ms
#define LOOP_RATE 	10.0f			// Hertz

using namespace rossc::core;
using namespace rossc::actor;

struct bumper_str {
	bool 	  	touched;
	TimeValue time;
};


bumper_str bumper;

void init_velocity_message(geometry_msgs::Twist& velocity) {
	velocity.linear.x = 0.0f;
	velocity.linear.y = 0.0f;
	velocity.linear.z = 0.0f;
	velocity.angular.x = 0.0f;
	velocity.angular.y = 0.0f;
	velocity.angular.z = 0.0f;
}

bool IsTouched(bumper_str* bumper_) {
	return bumper_->touched;
}

bool TimeElapsed(TimeValue* time_tic, double time_limit) {

	bool retcode = false;
	double time_elapsed = Time::Toc(time_tic);

	if(time_elapsed >= time_limit)
		retcode = true;
	
	return retcode;
}

void CB_bumper(const kobuki_msgs::BumperEventConstPtr& msg) {

	if (msg->state == true) {
		printf("[Robot] - Bumper!\n");
		
		bumper.touched = msg->state;
		Time::Tic(&bumper.time);  
	}
}

int main(int argc, char** argv) {
	
	ros::init(argc, argv, "robot");
	
	SystemControl sysctrl;
	
	TopicProxy<geometry_msgs::Twist, geometry_msgs::Twist> robot;	
	TopicProxy<rossc_messages::SystemMessage, std_msgs::Int16> system;	

	MapMessages<geometry_msgs::Twist> i_velocity;
	geometry_msgs::Twist velocity;

	ros::NodeHandle node;
	ros::Subscriber sub_bumper; 
	
	sub_bumper = node.subscribe("/mobile_base/events/bumper", 1, CB_bumper);
		
	robot.Register("device_navigation", TopicType::AsSubscriber);
	robot.Register("cmd_vel_mux/input/navi", TopicType::AsPublisher);

	ros::Rate loop_rate(LOOP_RATE);	
	
	ROS_INFO("[Robot] - Running");

	while(ros::ok()) {
		
		if(sysctrl.IsQuit()) {
			ROS_INFO("[Robot] - Is going to quit..");
			break;
		}
		
		if(sysctrl.IsPause()) {
			init_velocity_message(velocity);

			robot.Publish("cmd_vel_mux/input/navi", velocity);
			
			ros::spinOnce();
			loop_rate.sleep();
			continue;
		}
		
		
		if(IsTouched(&bumper) == true) {
			init_velocity_message(velocity);
			velocity.linear.x = -0.1f;

			robot.Publish("cmd_vel_mux/input/navi", velocity);
		
			if(TimeElapsed(&bumper.time, BACKWARD_TIME) == true) {
				bumper.touched = false;
				printf("[Robot] - Stop backward\n");
			}

			ros::spinOnce();
			loop_rate.sleep();
			continue;
		}
		
		
		robot.GetMessages(i_velocity);
		
		
		if (robot.HasUpdate("device_navigation")) {
				
			velocity = i_velocity["device_navigation"];
		

		
			robot.Publish("cmd_vel_mux/input/navi", velocity);
			
			//printf("Linear Velocity: %f\n", 
			//	i_velocity["device_navigation"].linear.x);

			//printf("Angle Velocity:  %f\n", 
			//	i_velocity["device_navigation"].angular.z);
		}



		ros::spinOnce();
		loop_rate.sleep();
	}
	
	ROS_INFO("[Robot] - Shutdown");

}

#include <iostream>

#include <ros/ros.h>
#include "rossc_fusion/rossc_fusion.h"


int main(int argc, char** argv) 
{

	ros::init(argc, argv, "test_fusion");


	rossc::fusion::FusionProxy proxy;

	proxy.Attach("i_keyboard");
	proxy.Attach("i_keyboard2");

	ros::Rate looprate(10);

	proxy.Start();


	while(ros::ok()) {

		ros::spinOnce();
		looprate.sleep();
	}


	proxy.Stop();
	proxy.Join();

	return 0;

}

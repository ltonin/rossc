#ifndef FUSIONPROXY_HPP
#define FUSIONPROXY_HPP

#include <string>
#include <unordered_map>
#include <ros/ros.h>

#include "rossc_core/Thread.hpp"
#include "rossc_core/Time.hpp"
#include "rossc_actor/MsgEntity.h"

#define DEFAULT_SLEEP 100

namespace rossc {
	namespace fusion {

struct entity {
	std::vector<float> strength;
	std::vector<float> direction;
	std::vector<float> width;
};

typedef std::unordered_map<std::string, entity> MapEntity;
typedef std::unordered_map<std::string, entity>::iterator MapEntityIt;

typedef std::unordered_map<std::string, ros::Subscriber> ProxySub;
typedef std::unordered_map<std::string, ros::Publisher> ProxyPub;


class FusionProxy : public rossc::core::Thread {
	public:
		FusionProxy(void);
		~FusionProxy(void);

		bool Attach(std::string name);
		bool Detach(std::string name);

		void Main(void);
		bool IsNew(void);

		void Dump(void);

	protected:
		void callback(const rossc_actor::MsgEntity& msg);
		void set_new(bool status);

	protected:
		ros::NodeHandle node_;
		ProxySub	prxsub_;
		ProxyPub	prxpub_;
		MapEntity 	entities_;

	private:

		bool is_new_;
		rossc::core::Semaphore sync_;




};


	}
}

#endif

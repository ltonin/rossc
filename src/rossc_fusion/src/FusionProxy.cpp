#ifndef FUSIONPROXY_CPP
#define FUSIONPROXY_CPP

#include "rossc_fusion/FusionProxy.hpp"

namespace rossc {
	namespace fusion {


FusionProxy::FusionProxy(void) {
	this->set_new(false);
}

FusionProxy::~FusionProxy(void) {
}


bool FusionProxy::Attach(std::string name) {

	ros::Subscriber sub;
	
	sub =  this->node_.subscribe(name, 1000, &FusionProxy::callback, this);

	this->prxsub_[name] = sub;

}

void FusionProxy::Main(void) {

	while(this->IsRunning()) {

		
		if(this->IsNew()) {
			this->Dump();
			this->set_new(false);
		}

		rossc::core::Time::Sleep(DEFAULT_SLEEP);

	}

}

bool FusionProxy::IsNew(void) {
	bool res;
	this->sync_.Wait();
	res = this->is_new_;
	this->sync_.Post();

	return res;
}


void FusionProxy::set_new(bool status) {
	this->sync_.Wait();
	this->is_new_= status;
	this->sync_.Post();
}

void FusionProxy::callback(const rossc_actor::MsgEntity& msg) {

	entity ent;

	ent.strength 	= msg.strength;
	ent.direction 	= msg.direction;
	ent.width 	= msg.width;

	this->sync_.Wait();
	this->entities_[msg.header.frame_id] = ent;
	this->sync_.Post();

	this->set_new(true);
}



void FusionProxy::Dump(void) {

	MapEntity 	mentity;
	MapEntityIt 	it;
	entity 		ent;
	std::vector<float>::iterator ite;

	this->sync_.Wait();
	mentity = this->entities_;
	this->sync_.Post();

	for(it = mentity.begin(); it != mentity.end(); it++) {
	
		printf("[FusionProxy] - Dump map entities\n");
		printf(" + %s\n", it->first.c_str());
		
		ent = it->second;

		for (ite = ent.strength.begin(); ite != ent.strength.end(); ite++) {
			printf(" |- Strengths: %f ", *ite);
		}
		printf("\n");
		
		for (ite = ent.direction.begin(); ite != ent.direction.end(); ite++) {
			printf(" |- Directions: %f ", *ite);
		}
		printf("\n");
		
		for (ite = ent.width.begin(); ite != ent.width.end(); ite++) {
			printf(" |- Sizes: %f ", *ite);
		}
		printf("\n");


	}


}



	}
}
#endif

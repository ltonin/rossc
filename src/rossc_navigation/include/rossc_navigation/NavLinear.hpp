#ifndef NAV_LINEAR_HPP
#define NAV_LINEAR_HPP

#include <vector>

#include "rossc_core/rossc_core.h"
#include "rossc_navigation/rossc_navigation.h"

#define MIN_FRONT_ANGLE 	-M_PI/20.0f 	// -20 degrees
#define MAX_FRONT_ANGLE  	M_PI/20.0f	// +20 degrees
#define MAX_LINEAR_VELOCITY 	0.15f		// 10 cm/s
#define DECAY_LINEAR_VELOCITY 	1.0f
#define TIME_CONSTANT_REPELLORS	1000.0f

namespace rossc {
	namespace navigation {

class NavLinear : public DynamicSystem {
	
	public:
		NavLinear(float velocity_max	= MAX_LINEAR_VELOCITY, 
			  float velocity_decay 	= DECAY_LINEAR_VELOCITY, 
			  float min_angle 	= MIN_FRONT_ANGLE, 
			  float max_angle 	= MAX_FRONT_ANGLE);
		virtual ~NavLinear(void);
		void Main(void);
	private:
		bool IsAtFront(float direction, float min_angle, float max_angle);
	private:
		float velocity_max_;
		float velocity_decay_;
		float min_angle_;
		float max_angle_;

		float loop_sleep_;
};

	}
}

#endif

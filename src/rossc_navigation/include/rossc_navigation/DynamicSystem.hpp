#ifndef DYNAMIC_SYSTEM_HPP
#define DYNAMIC_SYSTEM_HPP

#include <vector>

#include "rossc_core/rossc_core.h"
#include "rossc_actor/rossc_actor.h"

#define DEFAULT_SLEEP_MS 100.0f

namespace rossc {
	namespace navigation {


class DynamicSystem : public rossc::core::Thread {
	
	public:
		DynamicSystem(void);
		virtual ~DynamicSystem(void);
	
		bool GetState(float* value, std::string name);
		void SetState(float state, std::string name);

		virtual void AddObstacle(rossc::core::MapMessages<rossc::actor::ActorMessage>& map);

		virtual void AddTarget(rossc::actor::ActorMessage& msg){};
		virtual void ResetTarget(void){};
		virtual bool TargetReached(void){};
	protected:
		double Sleep(float ms, rossc::core::TimeValue* tmark);
		virtual float update(float state, float strength, float direction, float width);
	protected:

		
		rossc::core::MapMessages<rossc::actor::ActorMessage> obstacles_;
		rossc::core::Semaphore sync_obstacles_;

		std::unordered_map<std::string, float> state_;
		rossc::core::Semaphore sync_state_;


};

	}
}

#endif

#ifndef NAV_ANGULAR_HPP
#define NAV_ANGULAR_HPP

#include <vector>
#include <cmath>

#include "rossc_core/rossc_core.h"
#include "rossc_actor/rossc_actor.h"
#include "rossc_navigation/rossc_navigation.h"


#define TURN_RATE 1.0f 		/** Define the time [in seconds] needed to turn 
				  * in the following conditions:
				  * - No obstacles
				  * - One target
				  * - Turn angle +/- 45 degrees (+/- 0.785 rad)
				  */
#define TIME_CONSTANT_ATTRACTOR		1.0f
#define TIME_CONSTANT_REPELLORS		1000.0f


namespace rossc {
	namespace navigation {

struct Target {
	float strength;
	float direction;
	float width;
	bool enabled;
	double time_elapsed;
	rossc::core::TimeValue time_set;
};

class NavAngular : public DynamicSystem {

	public:
		NavAngular(float turnrate = TURN_RATE);
		virtual ~NavAngular(void);
		void Main(void);
		
		void AddTarget(rossc::actor::ActorMessage& msg);
		void ResetTarget(void);
		bool TargetReached(void);
	private:

		Target target_;
		rossc::core::Semaphore sync_target_;
		
		float loop_sleep_;
		float turnrate_;
};

	}
}

#endif


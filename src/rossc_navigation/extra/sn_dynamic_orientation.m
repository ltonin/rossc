clear all; clc;


TIMELIMIT = 10;     % seconds

SIGMA   = 1.0;
LAMBDA  = 1.0;

TARGET  = 45.0;     % degree
RTARGET = TARGET*pi/180.0;
INITIALSTATE = 0.0;


TIMESTEP  = 0.100;  % seconds
t = 0:TIMESTEP:TIMELIMIT;

state = INITIALSTATE*ones(length(t), 1);

for tId = 2:length(t)
    pstate = state(tId - 1);
    
    domega = RTARGET - pstate;
    garg   = - (domega.^2)/(2*SIGMA.^2);
    dphi   = LAMBDA*domega*exp(garg);
    state(tId)  = pstate + dphi;
end


plot(t, state*180.0/pi);
ylim([0 TARGET + 10]);
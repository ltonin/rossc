#ifndef DYNAMIC_SYSTEM_CPP
#define DYNAMIC_SYSTEM_CPP

#include "rossc_navigation/DynamicSystem.hpp"

namespace rossc {
	namespace navigation {

DynamicSystem::DynamicSystem(void) {}

DynamicSystem::~DynamicSystem(void) {}


bool DynamicSystem::GetState(float* state, std::string name) {

	bool retcode = false;

	this->sync_state_.Wait();
	auto it = this->state_.find(name);
	if (it != this->state_.end()) {
		retcode = true;
		*state = it->second;
	}
	this->sync_state_.Post();

	return retcode;
}

void DynamicSystem::SetState(float state, std::string name) {

	this->sync_state_.Wait();
	this->state_[name] = state;
	this->sync_state_.Post();
}


void DynamicSystem::AddObstacle(rossc::core::MapMessages<rossc::actor::ActorMessage>& map) {

	this->sync_obstacles_.Wait();
	for (auto it = map.begin(); it != map.end(); it++)
		this->obstacles_[it->first] = it->second;
	this->sync_obstacles_.Post();

}


float DynamicSystem::update(float state, float strength, float direction, float width) {

	float diff;
	float value = 0.0f;

	if (std::isnan(strength)) 
		strength = 0.0f;

	diff     = direction - state;
	value 	 += strength*diff*rossc::core::gaussian(diff, width);

	return value;
}

double DynamicSystem::Sleep(float ms, rossc::core::TimeValue* tmark) {

	rossc::core::TimeValue cmark;
	rossc::core::Time::Tic(&cmark);

	float sleep_ms;
	float passed_before;
	float passed_after;

	if (tmark == NULL) {
		passed_before = 0.0f;
	} else {
		passed_before = rossc::core::Time::Toc(tmark);
	}

	sleep_ms = ms - passed_before;

	if (sleep_ms >= 0.0f) {
		rossc::core::Time::Sleep(sleep_ms);
	} else {
		printf("[Warning] - Thread loop is going late\n");
		printf("            Loop execution takes %f ms instead of %f ms\n", passed_before, ms);
		rossc::core::Time::Sleep(DEFAULT_SLEEP_MS);
	}

	return (passed_before + rossc::core::Time::Toc(&cmark));

}


	}
}

#endif

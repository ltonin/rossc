#ifndef NAV_ANGULAR_CPP
#define NAV_ANGULAR_CPP

#include "rossc_navigation/NavAngular.hpp"

namespace rossc {
	namespace navigation {

NavAngular::NavAngular(float turnrate) {
	this->SetState(0.0f, "angular_velocity");
	this->SetState(0.0f, "angular_position");
	this->loop_sleep_ = 100.0f;
	this->turnrate_ = turnrate;
}

NavAngular::~NavAngular(void) {}

void NavAngular::Main(void) {
	
	rossc::core::TimeValue tmark;
	float dt = 0.0f;
	
	Target target;
	rossc::core::MapMessages<rossc::actor::ActorMessage> obs;

	float target_velocity;
	float obstacles_velocity;

	float cur_angle;
	float new_angle;
	float new_velocity;

	float turnrate = this->turnrate_;

	while(this->IsRunning()) {
		rossc::core::Time::Tic(&tmark);
	
		target_velocity 	= 0.0f;
		obstacles_velocity 	= 0.0f;

		// Get current obstacles map
		this->sync_obstacles_.Wait();
		obs = this->obstacles_;
		this->sync_obstacles_.Post();

		// Get current target
		this->sync_target_.Wait();
		target = this->target_;
		this->sync_target_.Post();

		// Get current angle
		this->GetState(&cur_angle, "angular_position");


		// Compute target influence
		if (target.enabled == true) {
			target_velocity += this->update(cur_angle, target.strength,
								   target.direction,
								   target.width);
		}

		// Compute obstacles influence
		for(auto it = obs.begin(); it != obs.end(); it++) {
			auto csize = it->second.strength.size();
			for (auto i = 0; i < csize; i++) {
				obstacles_velocity += this->update(0.0f, it->second.strength.at(i), 
						       			 it->second.direction.at(i), 
						       			 it->second.width.at(i));
			}
		}
	
		// Compute overall velocity
		new_velocity = TIME_CONSTANT_ATTRACTOR*target_velocity + 
			       TIME_CONSTANT_REPELLORS*obstacles_velocity;

		// Compute angle integrating the velocity
		//new_angle    = cur_angle + (dt/1000.0f)*TIME_CONSTANT_ATTRACTOR*target_velocity;
		new_angle    = cur_angle + (dt/1000.0f)*target_velocity*turnrate;

		// Set new velocity and angle
		this->SetState(new_velocity, "angular_velocity");
		this->SetState(new_angle, "angular_position");
		
		// Check if target is reached
		if (this->TargetReached()) {
			this->sync_target_.Wait();
			printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Target Reached in %f\n", this->target_.time_elapsed);
			this->sync_target_.Post();

			this->ResetTarget();
			this->SetState(0.0f, "angular_position");
		}
		
		dt = this->Sleep(this->loop_sleep_, &tmark);
		
	}
}

void NavAngular::AddTarget(rossc::actor::ActorMessage& msg) {

	this->sync_target_.Wait();
	this->target_.strength  = msg.strength.back();
	this->target_.direction = msg.direction.back();
	this->target_.width 	= msg.width.back();
	this->target_.enabled 	= true;
	rossc::core::Time::Tic(&this->target_.time_set);
	this->sync_target_.Post();

	this->SetState(0.0f, "angular_position");
	
	printf("Target Set:\n");
	printf("- Goal:	%f\n", msg.direction.back());
}

void NavAngular::ResetTarget(void) {
	this->sync_target_.Wait();
	this->target_.enabled = false;
	this->sync_target_.Post();
}

bool NavAngular::TargetReached(void) {

	float cangle;
	float cgoal;
	float cdiff;
	bool enabled;
	bool reached = false;

	
	this->sync_target_.Wait();
	enabled = this->target_.enabled;
	cgoal = this->target_.direction;
	this->sync_target_.Post();
	

	if (enabled == true) {

		this->GetState(&cangle, "angular_position");
		cdiff = fabs(cangle - cgoal);
		if (cdiff < 0.05f) {
			this->sync_target_.Wait();
			this->target_.time_elapsed = rossc::core::Time::Toc(&this->target_.time_set);
			this->sync_target_.Post();
			reached = true;
		}
	}

	return reached;
}


	}
}

#endif

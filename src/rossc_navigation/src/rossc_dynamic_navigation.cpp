#include <ros/ros.h>
#include "std_msgs/Int16.h"
#include <geometry_msgs/Twist.h>

#include "rossc_actor/rossc_actor.h"
#include "rossc_core/rossc_core.h"
#include "rossc_navigation/rossc_navigation.h"

#define LOOP_RATE 	10.0f			// Hertz

using namespace rossc::core;
using namespace rossc::actor;
using namespace rossc::navigation;

int main(int argc, char** argv) {

	ros::init(argc, argv, "dynamic_navigation");

	TopicProxy<ActorMessage, std_msgs::Int16> repellors;	
	TopicProxy<ActorMessage, std_msgs::Int16> attractors;	
	TopicProxy<std_msgs::Int16, geometry_msgs::Twist> navigation;	
	
	SystemControl sysctrl;
	
	MapMessages<ActorMessage> obstacles;
	MapMessages<ActorMessage> targets;
	geometry_msgs::Twist 	  velocity;

	float angular_position;
	float angular_velocity;
	float linear_velocity;
	NavAngular sys_angular(2.0f);
	NavLinear  sys_linear(0.1f, 1.0f);

	repellors.Register("actor_hokuyo", TopicType::AsSubscriber);
	repellors.Register("actor_kinectscan", TopicType::AsSubscriber);
	attractors.Register("actor_keyboard", TopicType::AsSubscriber);
	attractors.Register("actor_network", TopicType::AsSubscriber);
	navigation.Register("device_navigation", TopicType::AsPublisher);

	ros::Rate loop_rate(LOOP_RATE);	
	
	sys_angular.Start();
	sys_linear.Start();
	
	ROS_INFO("[DynamicNavigation] - Running");

	while(ros::ok()) {
		
		if(sysctrl.IsQuit()) {
			ROS_INFO("[DynamicNavigation] - Is going to quit..");
			break;
		}

	
		repellors.GetMessages(obstacles);
		attractors.GetMessages(targets);

		if (repellors.HasUpdate("actor_hokuyo")) {
			sys_angular.AddObstacle(obstacles);
			sys_linear.AddObstacle(obstacles);
		}
		
		if (repellors.HasUpdate("actor_kinectscan")) {
			sys_angular.AddObstacle(obstacles);
			sys_linear.AddObstacle(obstacles);
		}
		
		if (attractors.HasUpdate("actor_keyboard")) {
			sys_angular.AddTarget(targets["actor_keyboard"]);
			printf("Command keyboard\n");
		}
		
		if (attractors.HasUpdate("actor_network")) {
			sys_angular.AddTarget(targets["actor_network"]);
			printf("Command network\n");
		}
	
		sys_angular.GetState(&angular_position, "angular_position");
		sys_angular.GetState(&angular_velocity, "angular_velocity");
		sys_linear.GetState(&linear_velocity, "linear_velocity");

		velocity.angular.z = -angular_velocity;
		velocity.angular.y = 0.0f;
		velocity.angular.x = 0.0f;
		velocity.linear.z  = 0.0f;
		velocity.linear.y  = 0.0f;
		velocity.linear.x  = linear_velocity;

		navigation.Publish("device_navigation", velocity);

		printf("Linear Position: %f \t- Linear Velocity: %f\n", 0.0f, linear_velocity);
		printf("Angle Position:  %f \t- Angle Velocity:  %f\n", angular_position, angular_velocity);
		
		ros::spinOnce();
		loop_rate.sleep();
	
	}

	ROS_INFO("[DynamicNavigation] - Shutdown");

	return 0;
}

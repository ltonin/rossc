#ifndef NAV_LINEAR_CPP
#define NAV_LINEAR_CPP

#include "rossc_navigation/NavLinear.hpp"

namespace rossc {
	namespace navigation {

NavLinear::NavLinear(float velocity_max, float velocity_decay, float min_angle, float max_angle) {
	this->SetState(0.0f, "linear_velocity");
	this->loop_sleep_ = 100.0f;

	this->velocity_max_   = velocity_max;
	this->velocity_decay_ = velocity_decay;
	this->min_angle_      = min_angle;
	this->max_angle_      = max_angle;
}

NavLinear::~NavLinear(void) {}

void NavLinear::Main(void) {

	rossc::core::TimeValue tmark;
	float dt = 0.0f;
	
	rossc::core::MapMessages<rossc::actor::ActorMessage> obs;

	float obstacles_occupancy;
	float new_velocity;
	float strength, direction, width;

	float velocity_max   = this->velocity_max_;
	float velocity_decay = this->velocity_decay_;
	float min_angle      = this->min_angle_;
	float max_angle      = this->max_angle_;

	while(this->IsRunning()) {
		rossc::core::Time::Tic(&tmark);

		obstacles_occupancy = 0.0f;	
		
		// Get current obstacles map
		this->sync_obstacles_.Wait();
		obs = this->obstacles_;
		this->sync_obstacles_.Post();
		
		
		// Compute obstacles influence
		for(auto it = obs.begin(); it != obs.end(); it++) {
			auto csize = it->second.strength.size();
			for (auto i = 0; i < csize; i++) {
				if(this->IsAtFront(it->second.direction.at(i), min_angle, max_angle)) {
					
					strength = it->second.strength.at(i);
					direction = it->second.direction.at(i);
					width = it->second.width.at(i);

					if(std::isnan(strength)) {
						strength = 0.0f;
					}
						

					obstacles_occupancy += 
						strength*rossc::core::gaussian(direction, width);
				}
			}
		}

		obstacles_occupancy = TIME_CONSTANT_REPELLORS*obstacles_occupancy;
		new_velocity = velocity_max*exp(-(obstacles_occupancy)/(velocity_decay));
		
		// Set new velocity and angle
		this->SetState(new_velocity, "linear_velocity");
		
		dt = this->Sleep(this->loop_sleep_, &tmark);
	}
}

bool NavLinear::IsAtFront(float direction, float min_angle, float max_angle) {

	bool retcode = false;
	if (direction >= min_angle & direction <= max_angle)
		retcode = true;

	return retcode;
}



	}
}

#endif

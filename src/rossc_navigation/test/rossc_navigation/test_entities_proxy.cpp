#include <ros/ros.h>
#include "std_msgs/Int16.h"

#include "rossc_actor/rossc_actor.h"
#include "rossc_core/rossc_core.h"

#define LOOP_RATE 	10.0f			// Hertz
#define LOOP_PERIOD 	1000.0f/LOOP_RATE	// Milliseconds
#define LATE_TOLLERANCE	0.5f 			// Percentage

using namespace rossc::core;
using namespace rossc::actor;

int main(int argc, char** argv) {

	ros::init(argc, argv, "entities_proxy");

	TopicProxy<ActorMessage, std_msgs::Int16> repellors;	
	TopicProxy<ActorMessage, std_msgs::Int16> attractors;	
	
	MapMessages<ActorMessage> maprep;
	MapMessages<ActorMessage> mapatt;
	
	TimeValue ratecheck;
	
	repellors.Register("actor_hokuyo", TopicType::AsSubscriber);
	attractors.Register("actor_keyboard", TopicType::AsSubscriber);

	ros::Rate loop_rate(LOOP_RATE);	

	while(ros::ok()) {
	
		Time::Tic(&ratecheck);

		repellors.GetMessages(maprep);
		attractors.GetMessages(mapatt);

		if (repellors.HasUpdate("actor_hokuyo"))
				printf("[Repellors] - Message from actor_hokuyo\n");
		
		if (attractors.HasUpdate("actor_keyboard"))
				printf("[Attractors] - Message from actor_keyboard\n");

		ros::spinOnce();
		loop_rate.sleep();
	
		// DEBUG - LATENCY CHECK
		if((Time::Toc(&ratecheck) - LOOP_PERIOD) >= (LATE_TOLLERANCE * 100.0f)/(LOOP_PERIOD)) {
			printf("[Warning] - Running late:\n");
			printf(" 	  |- Actual Time:	%f\n", Time::Toc(&ratecheck));
			printf(" 	  |- Estimated Time:	%f\n", LOOP_PERIOD);
			printf(" 	  |- Tolerance:		%f%% \n", LATE_TOLLERANCE);
		}
	}

	return 0;
}

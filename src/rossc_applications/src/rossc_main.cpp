#include <ros/ros.h>

#include "rossc_core/rossc_core.h"
#include "rossc_messages/SystemMessage.h"
#include "rossc_actor/SystemControl.hpp"

#define LOOP_RATE 	10.0f			// Hertz

using namespace rossc::core;
using namespace rossc::actor;

int main(int argc, char** argv) {
	ros::init(argc, argv, "main");

	bool go_exit = false;
	SystemControl sysctrl;
	int next_state;

	ros::Rate loop_rate(LOOP_RATE);	
	
	ROS_INFO("[Main] - Running");

	while(ros::ok() && go_exit == false) {
		
		next_state = sysctrl.GetNextState();

		if(next_state != SystemControl::State::UNDEF) {
			sysctrl.SetState(next_state);
			ROS_INFO("[Main] - New state set: %s\n", sysctrl.GetStateName(next_state).c_str());
		}

		switch(sysctrl.GetState()) {
			case SystemControl::State::NAVIGATION:
				break;
			case SystemControl::State::PAUSE:
				break;
			case SystemControl::State::QUIT:
				go_exit = true;
				break;
			default:
				break;
		}
		ros::spinOnce();
		loop_rate.sleep();
	}

	ROS_INFO("[Main] - Shutdown");

	ros::shutdown();

}

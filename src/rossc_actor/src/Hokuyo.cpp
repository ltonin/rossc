#ifndef HOKUYO_CPP
#define HOKUYO_CPP

#include "rossc_actor/Hokuyo.hpp"

namespace rossc {
	namespace actor {

Hokuyo::Hokuyo(void) {
	this->SetUpdateTime(HOKUYO_SLEEP);
	this->hokuyo_topic_ = HOKUYO_TOPIC;
	this->hokuyo_sub_ = this->hokuyo_node_.subscribe(this->hokuyo_topic_, HOKUYO_MSG_SIZE, &Hokuyo::callback, this);
}

Hokuyo::~Hokuyo(void) {
}


void Hokuyo::Main(void) {
	while(this->IsRunning()) {
		rossc::core::Time::Sleep(10.0f);
		ros::spinOnce();
	}
}


bool Hokuyo::Get(std::vector<float>& scans) {

	bool isempty;

	this->sync_.Wait();
	isempty = this->scans_.empty();
	this->sync_.Post();


	if (isempty == false) {
		this->sync_.Wait();
		scans = this->scans_;
		this->sync_.Post();
	}

	return !isempty;
}

void Hokuyo::callback(const sensor_msgs::LaserScan::ConstPtr& msg) {
	
	this->sync_.Wait();
	this->scans_ 	 = msg->ranges;
	this->angle_min_ = msg->angle_min;
	this->angle_max_ = msg->angle_max;
	this->angle_inc_ = msg->angle_increment;
	this->range_min_ = msg->range_min;
	this->range_max_ = msg->range_max;
	this->sync_.Post();
}

	}
}



#endif

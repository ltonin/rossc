#ifndef NETSERVER_CPP
#define NETSERVER_CPP

#include "rossc_actor/NetServer.hpp"

namespace rossc	{
	namespace actor {


NetServer::NetServer(const char* address, int protocol) : Device(Device::AsNetwork)  {
	this->SetUpdateTime(50.0f);

	this->sync_.Wait();
	this->isnew_ = false;
	this->sync_.Post();
	
	this->server_  = new rossc::core::Server();	

	this->register_callback();
	
	if(server_->Bind(address, protocol))
		printf("[NetServer] Listening on %s\n", address);

}

NetServer::~NetServer(void) {
	this->server_->Release();
	delete this->server_;
}

void NetServer::register_callback(void) {
	Callback_Socket(this->server_->iOnRelease,    this, HandleRelease);
	Callback_Socket(this->server_->iOnAcceptPeer,     this, HandleAcceptPeer);
	Callback_Socket(this->server_->iOnDropPeer,       this, HandleDropPeer);
	Callback_Socket(this->server_->iOnRecv,       this, HandleRecv);
}

void NetServer::Main(void) {}

bool NetServer::Get(std::string& stream) {
	bool retcode = false;

	this->sync_.Wait();
	if(this->isnew_ == true) {
		stream = this->buffer_;
		this->isnew_ = false;
		retcode = true;
	}
	this->sync_.Post();

	return retcode;
}

void NetServer::HandleRecv(rossc::core::Socket* caller, rossc::core::Streamer* stream) { 
	rossc::core::Server* server = (rossc::core::Server*)caller;
	std::string buffer;
	stream->Extract(&buffer);
	stream->Clear();
	
	this->sync_.Wait();
	this->buffer_ = buffer;
	this->isnew_ = true;
	this->sync_.Post();
}

void NetServer::HandleAcceptPeer(rossc::core::Socket* caller, rossc::core::Address addr) {
	printf("[NetServer] Socket %d accepted %s\n", caller->GetFID(), addr.c_str());
}

void NetServer::HandleDropPeer(rossc::core::Socket* caller, rossc::core::Address addr) {
	printf("[NetServer] Socket %d dropped %s\n", caller->GetFID(), addr.c_str());
}


	}
}

#endif

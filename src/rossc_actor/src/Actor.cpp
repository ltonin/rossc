#ifndef ACTOR_CPP
#define ACTOR_CPP

#include "rossc_actor/Actor.hpp"

namespace rossc	{
	namespace actor {

Actor::Actor(void) {
}

Actor::~Actor(void) {
}


bool Actor::Setup(std::string topic) {

	this->SetTopic(topic);
	this->pub_ = this->node_.advertise<rossc::actor::ActorMessage>(this->GetTopic(), DEFAULT_MSG_SIZE);
	this->isset_=true;
	
}

void Actor::Publish(ActorMessage msg) {
	if(this->IsSet())
		this->pub_.publish(msg);
}

bool Actor::SetTopic(std::string topic) {
	this->topic_=topic;
}

std::string Actor::GetTopic(void) {
	return this->topic_;
}

bool Actor::IsSet(void) {
	return this->isset_;
}



	}
}


#endif

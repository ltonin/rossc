#ifndef DEVICE_CPP
#define DEVICE_CPP

#include "rossc_actor/Device.hpp"

namespace rossc	{
	namespace actor {

Device::Device(int type) {
	this->sync_.Wait();
	this->type_=type;
	this->sync_.Post();
}

Device::~Device(void) {
}

int Device::GetType(void) {
	int type;
	this->sync_.Wait();
	type = this->type_;
	this->sync_.Post();

	return type;
}


bool Device::Destroy(void) {
}

bool Device::SetUpdateTime(unsigned int ms) {
	this->sync_.Wait();
	this->updatetime_= ms;
	this->sync_.Post();
}

unsigned int Device::GetUpdateTime(void) {
	unsigned int ms;
	this->sync_.Wait();
	ms = this->updatetime_;
	this->sync_.Post();

	return ms;
}



	}
}

#endif

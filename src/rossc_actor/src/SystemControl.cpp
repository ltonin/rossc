#ifndef SYSTEMCONTROL_CPP
#define SYSTEMCONTROL_CPP

#include "rossc_actor/SystemControl.hpp"

namespace rossc {
	namespace actor {


SystemControl::SystemControl(std::string topic, unsigned int size) {
	
	this->sync_.Wait();
	this->topic_ 	   = topic;
	this->pub_   	   = this->node_.advertise<SystemMessage>(this->topic_, size);
	this->sub_   	   = this->node_.subscribe<SystemMessage>(this->topic_, size, &SystemControl::callback_system, this);
	this->state_ 	   = SystemControl::State::IDLE;
	this->state_next_  = SystemControl::State::UNDEF;
	this->sync_.Post();
}

SystemControl::~SystemControl(void) {}


int SystemControl::GetState(void) {
	this->sync_.Wait();
	int state = this->state_;
	this->sync_.Post();
	return state;
}

int SystemControl::GetNextState(void) {
	this->sync_.Wait();
	int state = this->state_next_;
	this->sync_.Post();
	return state;
}

void SystemControl::SetState(int state) {

	SystemMessage msg;
	msg.header.stamp    = ros::Time::now();
	msg.header.frame_id = "system-setstate";
	msg.state_next 	    = SystemControl::State::UNDEF;
	msg.state_isnew	    = true;

	this->sync_.Wait();
	msg.state = state;
	this->state_ = state;
	this->pub_.publish(msg);
	this->sync_.Post();
}

void SystemControl::SwitchToState(int state_next) {
	
	SystemMessage msg;
	msg.header.stamp    = ros::Time::now();
	msg.header.frame_id = "system-switchstate";
	msg.state_next 	    = state_next;
	msg.state_isnew	    = false;

	this->sync_.Wait();
	msg.state      = this->state_;	
	this->pub_.publish(msg);
	this->sync_.Post();
}

std::string SystemControl::GetStateName(int state) {

	std::string name;

	switch(state) {
		case SystemControl::State::UNDEF:
			name = "Undefined";
			break;
		case SystemControl::State::IDLE:
			name = "Idle";
			break;
		case SystemControl::State::NAVIGATION:
			name = "Navigation";
			break;
		case SystemControl::State::PAUSE:
			name = "Pause";
			break;
		case SystemControl::State::QUIT:
			name = "Quit";
			break;
		default:
			name = "Unknown";
			break;
	}

	return name;
}

void SystemControl::callback_system(const SystemMessage::ConstPtr& msg) {

	this->sync_.Wait();

	if(msg->state_isnew)
		this->state_ 	  = msg->state;
	
	this->state_next_ = msg->state_next;
	this->sync_.Post();
}

//SystemMessage SystemControl::ConvertToMessage(int status) {
//
//	SystemMessage msg;
//	msg.header.stamp = ros::Time::now();
//	msg.header.frame_id = "system";
//	
//	switch(status) {
//		
//		case 'p':
//			msg.state = SystemControl::State::PAUSE;
//			break;
//		case 'q':
//			msg.state = SystemControl::State::QUIT;	
//			break;
//		case 'r':
//			msg.state = SystemControl::State::NAVIGATION;	
//			break;
//		default:
//			break;
//	}
//
//	return msg;
//}

bool SystemControl::IsQuit(void) {
	bool retcode = false;
	
	this->sync_.Wait();
	if(this->state_ == SystemControl::State::QUIT)
		retcode = true;
	this->sync_.Post();
	return retcode;
}
	
bool SystemControl::IsPause(void) {
	bool retcode = false;
	this->sync_.Wait();
	if(this->state_ == SystemControl::State::PAUSE)
		retcode = true;
	this->sync_.Post();
	return retcode;
}

bool SystemControl::IsIdle(void) {
	bool retcode = false;
	this->sync_.Wait();
	if(this->state_ == SystemControl::State::IDLE)
		retcode = true;
	this->sync_.Post();
	return retcode;
}

bool SystemControl::IsNavigation(void) {
	bool retcode = false;
	this->sync_.Wait();
	if(this->state_ == SystemControl::State::NAVIGATION)
		retcode = true;
	this->sync_.Post();
	return retcode;
}




	}
}
#endif

#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/ActorKinectScan.hpp"
#include "rossc_messages/ActorMessage.h"
#include "rossc_actor/SystemControl.hpp"

#define LOOP_RATE 	10


using namespace rossc::core;
using namespace rossc::actor;
using namespace std;

int main(int argc, char** argv) {


	ros::init(argc, argv, "kinectscan");

	vector<float> scans;
	vector<float>::iterator it;
	unsigned int scanId;

	SystemControl sysctrl;
	ActorKinectScan kinect(1000.0f, 0.2f, 0.2f);
	ActorMessage  msg;

	ros::Rate looprate(LOOP_RATE);

	kinect.Setup("actor_kinect");
	kinect.Start();
	
	ROS_INFO("[ActorKinectScan] - Running");
	while(ros::ok() && kinect.IsRunning()) {

		if(sysctrl.IsQuit()) {
			ROS_INFO("[ActorKinectScan] - Is going to quit..");
			break;
		}
		
		if(kinect.Get(scans)) {
			msg = kinect.Convert(scans);
			kinect.Publish(msg);
		}
		looprate.sleep();
	}

	//kinect.Stop();
	//kinect.Join();
	
	ROS_INFO("[ActorKinectScan] - Shutdown");
	
	return 0;

}

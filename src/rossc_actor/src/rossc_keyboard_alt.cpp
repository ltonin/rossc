#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/Keyboard.hpp"
#include "rossc_messages/KeyboardMessage.h"

#define LOOP_RATE 	10
#define KEY_ESC		113
#define ROS_FRAME_ID	"keyboard_alt"

typedef rossc_messages::KeyboardMessage KeyboardMessage;

int main(int argc, char** argv) {


	ros::init(argc, argv, "keyboard_alt");
	ros::NodeHandle node;

	ros::Publisher pub = node.advertise<KeyboardMessage>("raw_keyboard_alt", 1000);

	rossc::actor::Keyboard keyboard;
	KeyboardMessage msg;
	msg.header.frame_id = ROS_FRAME_ID;
	bool exit = false;
	int key;

	keyboard.AddKey('j');
	keyboard.AddKey('l');
	keyboard.AddKey('i');
	keyboard.AddKey('k');
	keyboard.AddKey('p');

	keyboard.Start();

	ros::Rate looprate(LOOP_RATE);
	
	ROS_INFO("[keyboard] - Press Q to esc");
	while(ros::ok() & keyboard.IsRunning() & exit == false) {

	
		if(keyboard.Get(key)) {
			
			msg.key = key;

			msg.header.stamp = ros::Time::now();
			pub.publish(msg);
			ros::spinOnce();

			if(msg.key == KEY_ESC) {
				ROS_INFO("[keyboard] - User asked to quit");
				exit = true;
			}
		}

		looprate.sleep();
	}

	keyboard.Stop();
	keyboard.Join();
	
	return 0;

}

#ifndef ACTOR_KINECTSCAN_CPP
#define ACTOR_KINECTSCAN_CPP

#include "rossc_actor/ActorKinectScan.hpp"

namespace rossc	{
	namespace actor {


ActorKinectScan::ActorKinectScan(float pstrength, float pdecay, float pdiameter, std::string frame_id) {

	this->param_strength_ = pstrength;
	this->param_decay_    = pdecay;
	this->param_diameter_ = pdiameter;
	this->frame_id_	      = frame_id;
}

ActorKinectScan::~ActorKinectScan(void) {
}


ActorMessage ActorKinectScan::Convert(const std::vector<float>& scans) {
	ActorMessage msg;
	std::vector<float>::const_iterator it;
	unsigned int i = 0;
	auto nreads = scans.size();
	float cstrength, cangle, cwidth;

	std::string frame_id  = this->frame_id_;
	float param_strength  = this->param_strength_;
	float param_decay     = this->param_decay_;
	float param_diameter  = this->param_diameter_;
	
	for(it=scans.begin(); it != scans.end(); it++) {

		cangle = this->angle_min_ + i*this->angle_inc_;
		cwidth = atan( (tan(this->angle_inc_/2.0f)) + param_diameter/(param_diameter + *it));
		
		if (std::isnan(cwidth))
			cwidth = 0.1f;

		cstrength = param_strength*(1.0f/nreads)*exp(- (*it/param_decay));
		
		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = frame_id;
		msg.strength.push_back(cstrength);
		msg.direction.push_back(cangle);
		msg.width.push_back(cwidth);
		i++;
	}


	return msg;	
}

	}

}

#endif

#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/ActorKeyboard.hpp"
#include "rossc_messages/ActorMessage.h"
#include "rossc_actor/SystemControl.hpp"

#define LOOP_RATE 	100

#define KEYBOARD_FRAMEID	"keyboard"
#define KEYBOARD_STRENGHT 	5.0f
#define KEYBOARD_WIDTH		M_PI/9.0f  // 20 degrees

using namespace rossc::core;
using namespace rossc::actor;

ActorMessage convert_msg_navigation(int& key) {

	ActorMessage msg;

	msg.header.stamp = ros::Time::now();
	msg.header.frame_id = KEYBOARD_FRAMEID;
	msg.strength.push_back(KEYBOARD_STRENGHT);
	msg.width.push_back(KEYBOARD_WIDTH);
	
	switch(key) {
		
		case 'a':
			msg.direction.push_back(-0.785f);	//-45 deg
			break;
		case 'd':
			msg.direction.push_back(0.785f);	// 45 deg
			break;
		case 'w':
			msg.direction.push_back(0.0f);
			break;
		case 's':
			msg.direction.push_back(-3.14f);	// 180 deg
			break;
		default:
			break;
	}

	return msg;
};

int main(int argc, char** argv) {


	ros::init(argc, argv, "keyboard");

	bool exit = false;
	int key;

	ActorKeyboard keyboard;
	SystemControl sysctrl;
	ActorMessage  msg_navigation;

	ros::NodeHandle node;
	ros::Publisher pub_navigation;
	pub_navigation = node.advertise<ActorMessage>("actor_keyboard", 1);
	

	keyboard.Setup("actor_keyboard");
	
	keyboard.AddKey('a');
	keyboard.AddKey('d');
	keyboard.AddKey('w');
	keyboard.AddKey('s');
	keyboard.AddKey('p');
	keyboard.AddKey('q');
	keyboard.AddKey('r');

	keyboard.Start();
	
	ros::Rate looprate(LOOP_RATE);

	ROS_INFO("[ActorKeyboard] - Press 'q' to esc");
	
	while(ros::ok() && keyboard.IsRunning()) {
		
		if(sysctrl.IsQuit()) {
			ROS_INFO("[ActorKeyboard] - Is going to quit..");
			break;
		}

		if(keyboard.Get(key)) {

			switch(key) {
				case SystemControl::Key::Pause: 
					sysctrl.SwitchToState(SystemControl::State::PAUSE);
					break;
				case SystemControl::Key::Resume:
					sysctrl.SwitchToState(SystemControl::State::NAVIGATION);
					break;
				case SystemControl::Key::Quit: 
					sysctrl.SwitchToState(SystemControl::State::QUIT);
					break;
				default:
					msg_navigation = convert_msg_navigation(key);
					pub_navigation.publish(msg_navigation);
					break;
			}
		}
		
		ros::spinOnce();
		looprate.sleep();
	}

	//keyboard.Stop();
	//keyboard.Join();
	
	ROS_INFO("[ActorKeyboard] - Shutdown");
	
	return 0;

}

#include <iostream>

#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/ActorKeyboard.hpp"
#include "rossc_actor/NetServer.hpp"
#include "rossc_messages/ActorMessage.h"
#include "rossc_actor/SystemControl.hpp"

#define LOOP_RATE 	100
#define DEFAULT_IP	"0.0.0.0"
#define DEFAULT_PORT	"5555"

#define NETWORK_FRAMEID_KEYBOARD 	"network_keyboard"
#define NETWORK_FRAMEID_BCI 		"network_bci"
#define NETWORK_STRENGHT 	 	5.0f
#define NETWORK_WIDTH		 	M_PI/9.0f  // 20 degrees

using namespace rossc::actor;
using namespace rossc::core;
using namespace std;

ActorMessage convert_msg_navigation(int& key) {

	ActorMessage msg;

	msg.header.stamp = ros::Time::now();
	msg.header.frame_id = NETWORK_FRAMEID_KEYBOARD;
	msg.strength.push_back(NETWORK_STRENGHT);
	msg.width.push_back(NETWORK_WIDTH);
	
	switch(key) {
		
		case 'a':
			msg.direction.push_back(-0.785f);	//-45 deg
			break;
		case 'd':
			msg.direction.push_back(0.785f);	// 45 deg
			break;
		case 'w':
			msg.direction.push_back(0.0f);
			break;
		case 's':
			msg.direction.push_back(-3.14f);	// 180 deg
			break;
		case 11:
			msg.header.frame_id = NETWORK_FRAMEID_BCI;
			msg.direction.push_back(0.785f);	// 45 deg
			break;
		case 12:
			msg.header.frame_id = NETWORK_FRAMEID_BCI;
			msg.direction.push_back(-0.785f);	//-45 deg
			break;
		default:
			break;
	}

	return msg;
};

void usage(void) {
	printf("Usage: rossc_netkeyboard -p PORT\n\n");
	printf("  -p       Port to connect    [default: 5555]\n");
}

int main(int argc, char** argv) 
{
	int opt;

	string address(DEFAULT_IP);	
	string port(DEFAULT_PORT);
	
	while((opt = getopt(argc, argv, "p:")) != -1 ) {
		switch(opt) {
			case 'p':
				port.assign(optarg);
				break;
			default:
				usage();
				exit(0);
		}
	}
	
	address += ":" + port;

	ros::init(argc, argv, "network");
	

	int key;
	
	NetServer netserver(address.c_str());
	SystemControl sysctrl;
	
	std::string stream;
	ActorMessage  msg_navigation;

	ros::NodeHandle node;
	ros::Publisher pub_navigation;
	pub_navigation = node.advertise<ActorMessage>("actor_network", 1);
	
	ros::Rate looprate(LOOP_RATE);
	
	ROS_INFO("[ActorNetwork] - Running");

	while(ros::ok()) {
		if(sysctrl.IsQuit()) {
			ROS_INFO("[ActorNetwork] - Is going to quit..");
			break;
		}

		if(netserver.Get(stream)) {
			//key = *stream.begin();		
			key = (static_cast<int>(*stream.begin()) & 0xFF);
			switch(key) {
				case SystemControl::Key::Pause: 
					sysctrl.SwitchToState(SystemControl::State::PAUSE);
					break;
				case SystemControl::Key::Resume:
					sysctrl.SwitchToState(SystemControl::State::NAVIGATION);
					break;
				case SystemControl::Key::Quit: 
					sysctrl.SwitchToState(SystemControl::State::QUIT);
					break;
				default:
					msg_navigation = convert_msg_navigation(key);
					pub_navigation.publish(msg_navigation);
					break;
			}
		}

		ros::spinOnce();
		looprate.sleep();
	}

	ROS_INFO("[ActorNetwork] - Shutdown");

	return 0;
}

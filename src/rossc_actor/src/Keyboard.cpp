#ifndef KEYBOARD_CPP
#define KEYBOARD_CPP

#include "rossc_actor/Keyboard.hpp"

namespace rossc	{
	namespace actor {


Keyboard::Keyboard(void) : Device(Device::AsKeyboard) {
	this->store_settings();
	this->Reset();
	this->SetUpdateTime(KEYBOARD_SLEEP);
}

Keyboard::~Keyboard(void) {
	this->restore_settings();
}

bool Keyboard::Destroy(void) {
	if(this->IsRunning() == true) {
		this->Stop();
		this->Join();
	}

	return this->IsRunning();
}

void Keyboard::Main(void) {

	this->store_settings();
	
	while(this->IsRunning()) {
		
		this->get_key_pressed();

		rossc::core::Time::Sleep(this->GetUpdateTime());

	}

	this->restore_settings();
}

bool Keyboard::Get(int& value) {
	bool isnew 	= false;
	bool isvalid 	= false;
	int 	cval;
	KeySet 	cset;

	this->sync_.Wait();
	cval 	= this->key_;
	cset 	= this->keyset_;

	if(cval != Keyboard::NoValue) {
		isvalid = this->check_key(cval, cset);
	}

	if(isvalid == true) {
		isnew = true;
		value = cval;
	}

	this->sync_.Post();

	this->Reset();

	return isnew;

}

bool Keyboard::check_key(int cval, KeySet set) {

	bool isvalid = false;
	if (set.empty() == false) {
		for(auto it = set.begin(); it != set.end(); it++) {
			if(*it == cval) {
				isvalid = true;
			}
		}
	} else {
		isvalid = true;
	}

	return isvalid;
}

void Keyboard::Reset(void) {

	this->sync_.Wait();
	this->key_ = Keyboard::NoValue;
	this->sync_.Post();

}

void Keyboard::AddKey(char key) {
	this->sync_.Wait();
	this->keyset_.insert(key);
	this->sync_.Post();
}

void Keyboard::RemoveKey(char key) {
	this->sync_.Wait();
	this->keyset_.erase(key);
	this->sync_.Post();
}

void Keyboard::get_key_pressed(void) {
	int c = 0;

	struct termios org_opts, new_opts;
	int res = 0;

	//-----  store old settings -----------
	res = tcgetattr(STDIN_FILENO, &org_opts);
	assert(res == 0);
	//---- set new terminal parms --------
	memcpy(&new_opts, &org_opts, sizeof(new_opts));
	new_opts.c_lflag &=
	    ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOPRT | ECHOKE |
	      ICRNL);
	tcsetattr(STDIN_FILENO, TCSANOW, &new_opts);
	c = getchar();

	//------  restore old settings ---------
	res = tcsetattr(STDIN_FILENO, TCSANOW, &org_opts);
	assert(res == 0);

	this->sync_.Wait();
	this->key_ = c;
	this->sync_.Post();

}


void Keyboard::store_settings(void) {

	int res = 0;
	struct termios std_opts_;

	this->sync_opts_.Wait();

	res = tcgetattr(STDIN_FILENO, &std_opts_);
	this->std_opts_ = std_opts_;
	assert(res == 0);

	this->sync_opts_.Post();
}

void Keyboard::restore_settings(void) {

	int res = 0;

	this->sync_opts_.Wait();
	
	res = tcsetattr(STDIN_FILENO, TCSANOW, &this->std_opts_);
	assert(res == 0);

	this->sync_opts_.Post();
}

	}
}

#endif

#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/Hokuyo.hpp"

#define LOOP_RATE 	10

int main(int argc, char** argv) {


	ros::init(argc, argv, "laser");

	rossc::actor::Hokuyo hokuyo;
	std::vector<float> scans;
	std::vector<float>::iterator it;
	unsigned int scanId;
	
	hokuyo.Start();

	ros::Rate looprate(LOOP_RATE);
	
	ROS_INFO("[hokuyo] - Running");
	while(ros::ok() & hokuyo.IsRunning()) {
		if(hokuyo.Get(scans)) {
			scanId = 0;
			for(it=scans.begin(); it != scans.end(); it++) {
				printf("[%d] - %f\n", scanId, *it);
				scanId++;
			}
		}

		looprate.sleep();
	}
	ROS_INFO("[hokuyo] - Exit");

	hokuyo.Stop();
	hokuyo.Join();
	
	return 0;

}

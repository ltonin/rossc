#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/KinectScan.hpp"

#define LOOP_RATE 	10

int main(int argc, char** argv) {


	ros::init(argc, argv, "laser");

	rossc::actor::KinectScan kinect;
	std::vector<float> scans;
	std::vector<float>::iterator it;
	unsigned int scanId;
	
	kinect.Start();

	ros::Rate looprate(LOOP_RATE);
	
	ROS_INFO("[kinect] - Running");
	while(ros::ok() & kinect.IsRunning()) {
		if(kinect.Get(scans)) {
			scanId = 0;
			for(it=scans.begin(); it != scans.end(); it++) {
				printf("[%d] - %f\n", scanId, *it);
				scanId++;
			}
		}

		looprate.sleep();
	}
	ROS_INFO("[hokuyo] - Exit");

	kinect.Stop();
	kinect.Join();
	
	return 0;

}

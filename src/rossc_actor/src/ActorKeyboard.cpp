#ifndef ACTOR_KEYBOARD_CPP
#define ACTOR_KEYBOARD_CPP

#include "rossc_actor/ActorKeyboard.hpp"

namespace rossc	{
	namespace actor {


ActorKeyboard::ActorKeyboard(void) {
	this->store_settings();
	this->Reset();
	this->SetUpdateTime(KEYBOARD_SLEEP);
}

ActorKeyboard::~ActorKeyboard(void) {
	this->restore_settings();
}

ActorMessage ActorKeyboard::Convert(int& key) {
	
	ActorMessage msg;

	msg.header.stamp = ros::Time::now();
	msg.header.frame_id = KEYBOARD_FRAMEID;
	msg.strength.push_back(KEYBOARD_STRENGHT);
	msg.width.push_back(KEYBOARD_WIDTH);
	
	switch(key) {
		
		case 'a':
			msg.direction.push_back(-0.785f);	//-45 deg
			break;
		case 'd':
			msg.direction.push_back(0.785f);	// 45 deg
			break;
		case 'w':
			msg.direction.push_back(0.0f);
			break;
		case 's':
			msg.direction.push_back(-3.14f);	// 180 deg
			break;
		default:
			break;
	}
	
	return msg;	

}

	}

}

#endif

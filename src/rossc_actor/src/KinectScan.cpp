#ifndef KINECTSCAN_CPP
#define KINECTSCAN_CPP

#include "rossc_actor/KinectScan.hpp"

namespace rossc {
	namespace actor {

KinectScan::KinectScan(void) {
	this->SetUpdateTime(KINECTSCAN_SLEEP);
	this->kinectscan_topic_ = KINECTSCAN_TOPIC;
	this->kinectscan_sub_ = this->kinectscan_node_.subscribe(this->kinectscan_topic_, KINECTSCAN_MSG_SIZE, &KinectScan::callback, this);
}

KinectScan::~KinectScan(void) {
}


void KinectScan::Main(void) {
	while(this->IsRunning()) {
		rossc::core::Time::Sleep(10.0f);
		ros::spinOnce();
	}
}


bool KinectScan::Get(std::vector<float>& scans) {

	bool isempty;

	this->sync_.Wait();
	isempty = this->scans_.empty();
	this->sync_.Post();


	if (isempty == false) {
		this->sync_.Wait();
		scans = this->scans_;
		this->sync_.Post();
	}

	return !isempty;
}

void KinectScan::callback(const sensor_msgs::LaserScan::ConstPtr& msg) {
	
	this->sync_.Wait();
	this->scans_ 	 = msg->ranges;
	this->angle_min_ = msg->angle_min;
	this->angle_max_ = msg->angle_max;
	this->angle_inc_ = msg->angle_increment;
	this->range_min_ = msg->range_min;
	this->range_max_ = msg->range_max;
	this->sync_.Post();
}

	}
}



#endif

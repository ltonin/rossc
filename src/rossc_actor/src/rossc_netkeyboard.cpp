#include <iostream>
#include <unistd.h>

#include "rossc_core/SocketProxy.hpp"
#include "rossc_core/Client.hpp"
#include "rossc_actor/Keyboard.hpp"
#include "rossc_core/Time.hpp"

#define DEFAULT_WAIT 	100
#define KEY_Q 		113
#define DEFAULT_IP	"0.0.0.0"
#define DEFAULT_PORT	"5555"

using namespace rossc::core;
using namespace std;

class EventHandler : public SocketProxy {};

void RegisterAll(Client* client, EventHandler *handler) {
	Callback_Socket(client->iOnConnect,    handler, HandleConnect);
	Callback_Socket(client->iOnDisconnect, handler, HandleDisconnect);
	Callback_Socket(client->iOnRecv,   	 handler, HandleRecv);
}

void usage(void) {
	printf("Usage: rossc_netkeyboard -a IP -p PORT\n\n");
	printf("  -a       Address to connect [default: 0.0.0.0]\n");
	printf("  -p       Port to connect    [default: 5555]\n");
}

int main(int argc, char** argv) 
{
	int opt;

	string address(DEFAULT_IP);
	string port(DEFAULT_PORT);
	
	while((opt = getopt(argc, argv, "a:p:")) != -1 ) {
		switch(opt) {
			case 'a':
				address.assign(optarg);
				break;
			case 'p':
				port.assign(optarg);
				break;
			default:
				usage();
				exit(0);
		}
	}

	Client* client = new Client();
	EventHandler* handler = new EventHandler();

	RegisterAll(client, handler);
	Endpoint peer(address.c_str(), port.c_str());

	rossc::actor::Keyboard keyboard;
	int key;
	bool exit = false;

	keyboard.AddKey('a');
	keyboard.AddKey('d');
	keyboard.AddKey('w');
	keyboard.AddKey('s');

	keyboard.AddKey('p');
	keyboard.AddKey('q');
	keyboard.AddKey('r');

	keyboard.Start();

	while(client->Connect(peer.GetAddress()) == false) {
		printf("[netkeyboard] Waiting for connection...\n");
		Time::Sleep(1000);
	}
	
	printf("[netkeyboard] Connected to %s:%s\n", address.c_str(), port.c_str());
	
	std::cout<<"[netkeyboard] - Press 'q' to esc"<<std::endl;
	std::cout<<"[netkeyboard] - Available keys:"<<std::endl;
	std::cout<<"                Navigation: 'a', 'd', 'w', 's'"<<std::endl;
	std::cout<<"                System:     'p', 'q', 'r'"<<std::endl;
		
	while(exit == false && client->IsConnected()) {
		

		if(keyboard.Get(key)) {

			switch(key) {
				case KEY_Q:
					std::cout<<"[netkeyboard] - User asked to quit"<<std::endl;
					exit = true;
					break;
				default:
					std::cout<<"Key: "<<(char)key<<std::endl;
					break;
			}
			
			client->Send((const char*)&key);
		}
		

		rossc::core::Time::Sleep(DEFAULT_WAIT);

	}

	if(client->IsConnected())
		client->Disconnect();



	keyboard.Stop();
	keyboard.Join();

	delete client;
	
	return 0;

}

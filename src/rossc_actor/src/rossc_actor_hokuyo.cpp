#include "ros/ros.h"
#include "rossc_core/Time.hpp"
#include "rossc_actor/ActorHokuyo.hpp"
#include "rossc_messages/ActorMessage.h"
#include "rossc_actor/SystemControl.hpp"

#define LOOP_RATE 	10

using namespace rossc::core;
using namespace rossc::actor;
using namespace std;

int main(int argc, char** argv) {


	ros::init(argc, argv, "hokuyo");

	vector<float> scans;
	vector<float>::iterator it;
	unsigned int scanId;

	SystemControl sysctrl;
	ActorHokuyo hokuyo(1.0f, 0.3f, 0.15f);
	ActorMessage  msg;

	ros::Rate looprate(LOOP_RATE);

	hokuyo.Setup("actor_hokuyo");
	
	hokuyo.Start();
	
	ROS_INFO("[ActorHokuyo] - Running");
	while(ros::ok() && hokuyo.IsRunning()) {
		
		if(sysctrl.IsQuit()) {
			ROS_INFO("[ActorHokuyo] - Is going to quit..");
			break;
		}
		
		if(hokuyo.Get(scans)) {
			msg = hokuyo.Convert(scans);
			hokuyo.Publish(msg);
		}
		looprate.sleep();
	}

	//hokuyo.Stop();
	//hokuyo.Join();
	
	ROS_INFO("[ActorHokuyo] - Shutdown");
	
	return 0;

}

#ifndef LASER_HPP
#define LASER_HPP

#include "rossc_actor/Device.hpp"

namespace rossc {
	namespace actor {

class Laser : public Device {
	public:
		Laser(void);
		virtual ~Laser(void);
		virtual void Main(void);
};

	}
}


#endif

#ifndef KINECTSCAN_HPP
#define KINECTSCAN_HPP

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include "rossc_actor/Laser.hpp"

#define KINECTSCAN_MSG_SIZE 	1000
#define KINECTSCAN_SLEEP	100
#define KINECTSCAN_TOPIC 	"/scan"

namespace rossc {
	namespace actor {

class KinectScan : public Laser {
	public:
		KinectScan(void);
		virtual ~KinectScan(void);
		void Main(void);
		virtual bool Get(std::vector<float>& scans);

	public:
		void callback(const sensor_msgs::LaserScan::ConstPtr& msg);

	protected:
		std::vector<float> 	scans_;
		float angle_min_;
		float angle_max_;
		float angle_inc_;
		float range_min_;
		float range_max_;
	private:
		ros::Subscriber 	kinectscan_sub_;
		ros::NodeHandle 	kinectscan_node_;
		std::string 		kinectscan_topic_;

};

	}
}


#endif

#ifndef ACTOR_KINECTSCAN_HPP
#define ACTOR_KINECTSCAN_HPP

#include <ros/ros.h>

#include "rossc_actor/KinectScan.hpp"
#include "rossc_actor/Actor.hpp"
#include "rossc_messages/ActorMessage.h"

#define KINECTSCAN_FRAMEID		"kinectscan"
#define KINECTSCAN_SPATIAL_DECAY 	0.5f
#define KINECTSCAN_OVERALL_STRENGTH 	200.0f
#define ROBOT_DIAMETER 			0.354

namespace rossc	{
	namespace actor {

class ActorKinectScan : public KinectScan, public Actor {

	public: 
		ActorKinectScan(float pstrength      = KINECTSCAN_OVERALL_STRENGTH, 
			    	float pdecay         = KINECTSCAN_SPATIAL_DECAY, 
			    	float pdiameter      = ROBOT_DIAMETER,
				std::string frame_id = KINECTSCAN_FRAMEID);
		virtual ~ActorKinectScan(void);
		ActorMessage Convert(const std::vector<float>& scans);

	private:
		std::string frame_id_;
		float param_strength_;
		float param_decay_;
		float param_diameter_;
};

	}
}

#endif

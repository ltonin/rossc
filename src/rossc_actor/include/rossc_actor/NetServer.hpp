#ifndef NETSERVER_HPP
#define NETSERVER_HPP

#include <string.h>
#include <stdio.h>
#include <unordered_set>

#include "rossc_core/Socket.hpp"
#include "rossc_core/SocketProxy.hpp"
#include "rossc_core/Server.hpp"
#include "rossc_actor/Device.hpp"

namespace rossc	{
	namespace actor {


class NetServer : public Device, public rossc::core::SocketProxy {

	public: 
		NetServer(const char* address = "0.0.0.0:8000", int protocol = rossc::core::Socket::UDP);
		virtual ~NetServer(void);
		void Main(void);
		bool Get(std::string& stream);

	protected:
		void HandleRecv(rossc::core::Socket* caller, rossc::core::Streamer* stream);
		void HandleAcceptPeer(rossc::core::Socket* caller, rossc::core::Address addr);
		void HandleDropPeer(rossc::core::Socket* caller, rossc::core::Address addr);
	private:
		void register_callback(void);
	
	protected:
		std::string buffer_;
		bool isnew_;
	private:
		rossc::core::Server* server_;
		

};

	}
}

#endif

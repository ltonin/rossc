#ifndef ACTOR_HPP
#define ACTOR_HPP

#include <ros/ros.h>

#include "rossc_messages/ActorMessage.h"

#define DEFAULT_MSG_SIZE 1000

namespace rossc	{
	namespace actor {

typedef rossc_messages::ActorMessage ActorMessage;

class Actor {
	public:
		Actor(void);
		virtual ~Actor(void);
		
		bool Setup(std::string topic);

		bool SetTopic(std::string topic);
		std::string GetTopic(void);
		bool IsSet(void);
		
		virtual void Publish(ActorMessage msg);

	private:
		ros::Publisher 	pub_;
		ros::NodeHandle node_;
		std::string topic_;
		bool isset_;

};

	}
}


#endif

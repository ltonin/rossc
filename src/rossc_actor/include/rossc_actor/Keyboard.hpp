#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

#include <termios.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unordered_set>

#include "rossc_actor/Device.hpp"

#define KEYBOARD_SLEEP 100

namespace rossc	{
	namespace actor {


typedef std::unordered_set<char> KeySet;

class Keyboard : public Device {

	public: 
		Keyboard(void);
		virtual ~Keyboard(void);
		virtual bool Get(int& value);
		void AddKey(char key);
		void RemoveKey(char key);
		virtual void Main(void);
		bool Destroy(void);
		void Reset(void);

	protected:
		void get_key_pressed(void);
		void store_settings(void);
		void restore_settings(void);
		bool check_key(int value, KeySet set);

	public:
		const static int NoValue = -1;

	protected:
		int key_;
		KeySet keyset_;
		struct termios std_opts_;
		rossc::core::Semaphore sync_opts_;
		

};

	}
}

#endif

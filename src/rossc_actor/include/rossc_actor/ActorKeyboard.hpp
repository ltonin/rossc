#ifndef ACTOR_KEYBOARD_HPP
#define ACTOR_KEYBOARD_HPP

#include <cmath>

#include "rossc_actor/Actor.hpp"
#include "rossc_actor/Keyboard.hpp"
#include "rossc_messages/ActorMessage.h"

#define KEYBOARD_FRAMEID	"keyboard"
#define KEYBOARD_STRENGHT 	5.0f
#define KEYBOARD_WIDTH		M_PI/9.0f  // 20 degrees

namespace rossc	{
	namespace actor {

class ActorKeyboard : public Keyboard, public Actor {

	public: 
		ActorKeyboard(void);
		virtual ~ActorKeyboard(void);
		ActorMessage Convert(int& key);


};

	}
}

#endif

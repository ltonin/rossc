#ifndef SYSTEMCONTROL_HPP
#define SYSTEMCONTROL_HPP

#include <string>

#include "ros/ros.h"
#include "rossc_core/Semaphore.hpp"
#include "rossc_messages/SystemMessage.h"

namespace rossc {
	namespace actor {

typedef rossc_messages::SystemMessage SystemMessage;


class SystemControl {
	public:
		SystemControl(std::string topic = "system_info", unsigned int size = 1);
		virtual ~SystemControl(void);

		int  GetState(void);
		void SetState(int state);
		void SwitchToState(int state);
		int  GetNextState(void);
		std::string GetStateName(int state);

		//SystemMessage ConvertToMessage(int value);
		bool IsQuit(void);
		bool IsPause(void);
		bool IsIdle(void);
		bool IsNavigation(void);

	protected:
		void callback_system(const SystemMessage::ConstPtr& msg);

	public:
		enum State {
				UNDEF,		// 0
				IDLE, 		// 1
		      		NAVIGATION, 	// 2
		      		PAUSE, 		// 3
		      		QUIT		// 4
		};
		
		enum Key {
				Pause 	= 112,
				Quit 	= 113,
				Resume 	= 114
		};
	private:
		ros::NodeHandle node_;
		ros::Publisher  pub_;
		ros::Subscriber sub_;
		std::string 	topic_;
		rossc::core::Semaphore sync_;
	
		int state_;
		int state_next_;
		

};

	}
}

#endif

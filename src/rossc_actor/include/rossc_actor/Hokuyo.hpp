#ifndef HOKUYO_HPP
#define HOKUYO_HPP

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include "rossc_actor/Laser.hpp"

#define HOKUYO_MSG_SIZE 1000
#define HOKUYO_SLEEP	100
#define HOKUYO_TOPIC 	"/scan"

namespace rossc {
	namespace actor {

class Hokuyo : public Laser {
	public:
		Hokuyo(void);
		virtual ~Hokuyo(void);
		void Main(void);
		virtual bool Get(std::vector<float>& scans);

	public:
		void callback(const sensor_msgs::LaserScan::ConstPtr& msg);

	protected:
		std::vector<float> 	scans_;
		float angle_min_;
		float angle_max_;
		float angle_inc_;
		float range_min_;
		float range_max_;
	private:
		ros::Subscriber 	hokuyo_sub_;
		ros::NodeHandle 	hokuyo_node_;
		std::string 		hokuyo_topic_;

};

	}
}


#endif

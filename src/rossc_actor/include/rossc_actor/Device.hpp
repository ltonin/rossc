#ifndef DEVICE_HPP
#define DEVICE_HPP

#include <rossc_core/Thread.hpp>
#include <rossc_core/Time.hpp>

namespace rossc	{
	namespace actor {

class Device : public rossc::core::Thread {

	public:
		virtual ~Device(void);
		int GetType(void);
		bool SetUpdateTime(unsigned int ms);
		unsigned int GetUpdateTime(void);	

	protected:
		Device(int type);
		virtual void Main(void) = 0;
		virtual bool Destroy(void);

	public:
		typedef enum {
			AsKeyboard, 
			AsMouse,
			AsRobot,
			AsNetwork,
			AsLaser,
			AsCamera,
		} Type;

	protected:
		rossc::core::Semaphore sync_;
	
	private:
		int type_;
		unsigned int updatetime_;

};

	}
}

#endif

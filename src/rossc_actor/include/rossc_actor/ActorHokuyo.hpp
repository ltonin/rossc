#ifndef ACTOR_HOKUYO_HPP
#define ACTOR_HOKUYO_HPP

#include <ros/ros.h>

#include "rossc_actor/Hokuyo.hpp"
#include "rossc_actor/Actor.hpp"
#include "rossc_messages/ActorMessage.h"

#define HOKUYO_FRAMEID		"hokuyo"
#define HOKUYO_SPATIAL_DECAY 	0.5f
#define HOKUYO_OVERALL_STRENGTH 200.0f
#define ROBOT_DIAMETER 		0.354

namespace rossc	{
	namespace actor {

class ActorHokuyo : public Hokuyo, public Actor {

	public: 
		ActorHokuyo(float pstrength 	 = HOKUYO_OVERALL_STRENGTH, 
			    float pdecay    	 = HOKUYO_SPATIAL_DECAY, 
			    float pdiameter 	 = ROBOT_DIAMETER,
			    std::string frame_id = HOKUYO_FRAMEID);
		virtual ~ActorHokuyo(void);
		ActorMessage Convert(const std::vector<float>& scans);

	private:
		std::string frame_id_;
		float param_strength_;
		float param_decay_;
		float param_diameter_;
};

	}
}

#endif

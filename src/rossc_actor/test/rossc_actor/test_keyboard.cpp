#include <iostream>

#include "rossc_actor/Keyboard.hpp"
#include "rossc_core/Time.hpp"

#define DEFAULT_WAIT 	100
#define KEY_Q 		113

int main(int argc, char** argv) 
{

	rossc::actor::Keyboard keyboard;
	int key;
	bool exit = false;
	
	keyboard.Start();

	std::cout<<"[test_keyboard] - Press Q to esc"<<std::endl;


	while(exit == false) {

		if(keyboard.Get(key)) {

			switch(key) {
				case KEY_Q:
					std::cout<<"[test_keyboard] - User asked to quit"<<std::endl;
					exit = true;
					break;
				default:
					std::cout<<"Key: "<<(char)key<<std::endl;
					break;
			}
		}

		rossc::core::Time::Sleep(DEFAULT_WAIT);

	}


	keyboard.Stop();
	keyboard.Join();

	return 0;

}

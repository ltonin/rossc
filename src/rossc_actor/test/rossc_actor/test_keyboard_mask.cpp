#include <iostream>

#include "rossc_actor/Keyboard.hpp"
#include "rossc_core/Time.hpp"

#define DEFAULT_WAIT 	100
#define KEY_Q 		113

int main(int argc, char** argv) 
{

	rossc::actor::Keyboard keyboard;
	int key;
	bool exit = false;

	keyboard.AddKey('a');
	keyboard.AddKey('d');
	keyboard.AddKey('w');
	keyboard.AddKey('s');
	keyboard.AddKey('q');


	keyboard.Start();

	std::cout<<"[test_keyboard_mask] - Press 'q' to esc"<<std::endl;
	std::cout<<"[test_keyboard_mask] - Available keys: 'a', 'd', 'w', 's', 'q' "<<std::endl;


	while(exit == false) {

		if(keyboard.Get(key)) {

			switch(key) {
				case KEY_Q:
					std::cout<<"[test_keyboard_mask] - User asked to quit"<<std::endl;
					exit = true;
					break;
				default:
					std::cout<<"Key: "<<(char)key<<std::endl;
					break;
			}
		}

		rossc::core::Time::Sleep(DEFAULT_WAIT);

	}


	keyboard.Stop();
	keyboard.Join();

	return 0;

}

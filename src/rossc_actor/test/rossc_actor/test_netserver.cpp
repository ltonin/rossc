#include <iostream>
#include <unistd.h>

#include "rossc_actor/NetServer.hpp"
#include "rossc_core/Time.hpp"

#define DEFAULT_WAIT 	100
#define KEY_Q 		113
#define DEFAULT_IP 	"0.0.0.0"
#define DEFAULT_PORT	"5555"

void usage(void) {
	printf("Usage: test_netserver -p PORT\n\n");
	printf("  -p       Port to connect    [default: 5555]\n");
}

int main(int argc, char** argv) 
{
	int opt;

	std::string address(DEFAULT_IP);
	std::string port(DEFAULT_PORT);
	
	while((opt = getopt(argc, argv, "p:")) != -1 ) {
		switch(opt) {
			case 'p':
				port.assign(optarg);
				break;
			default:
				usage();
				exit(0);
		}
	}


	address += ":" + port;
	
	rossc::actor::NetServer netserver(address.c_str());
	std::string stream;
	int key;
	bool exit = false;
	

	while(exit == false) {

		if(netserver.Get(stream)) {
			//key = *stream.begin();		
			key = (static_cast<int>(*stream.begin()) & 0xFF);
			switch(key) {
				case KEY_Q:
					std::cout<<"[test_netserver] - User asked to quit"<<std::endl;
					exit = true;
					break;
				default:
					std::cout<<"Key: "<<key<<std::endl;
					break;
			}
		}

		rossc::core::Time::Sleep(DEFAULT_WAIT);

	}



	return 0;

}
